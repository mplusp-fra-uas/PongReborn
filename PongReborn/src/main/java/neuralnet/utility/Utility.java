package neuralnet.utility;


/**
 * A class containing all kinds of utility methods.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class Utility {

	
	/**
	 * Prints out a given double array.
	 * 
	 * @param array the double array
	 */
	public static void print(double[] array) {
		
		System.out.print("{ ");
		for (int i = 0; i < array.length; i++) {
			if (i < array.length - 1) {
				System.out.print(array[i] + ", ");
			} else {
				System.out.print(array[i] + " ");
			}
		}
		System.out.print("}");
	}
	
	
	/**
	 * Prints out a given double array with a line ending at the end.
	 * 
	 * @param array the double array
	 */
	public static void println(double[] array) {
		
		print(array);
		System.out.println();
	}

	
	/**
	 * Outputs a string representation of the given array.
	 * 
	 * @param array the double array
	 * @return a string representation of the given array
	 */
	public static String toString(double[] array) {
		
		StringBuilder stringBuilder = new StringBuilder();
		
		stringBuilder.append("{ ");
		for (int i = 0; i < array.length; i++) {
			if (i < array.length - 1) {
				stringBuilder.append(array[i] + ", ");
			} else {
				stringBuilder.append(array[i]+ " ");
			}
		}
		stringBuilder.append("}");
		
		return stringBuilder.toString();
	}
}
