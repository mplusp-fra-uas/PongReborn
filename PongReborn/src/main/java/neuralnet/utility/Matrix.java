package neuralnet.utility;

import java.util.Random;
import java.util.Scanner;

import neuralnet.base.Activation;

/**
 * A class representing a matrix containing double values.
 * Used to simplify matrix operations needed for our neural network.
 * <br>
 * Caution: This is far from a complete matrix implementation right now!
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class Matrix {
	private int rows;
	private int columns;
	private double[][] data;
	
	
	/**
	 * Constructor. Creates a new matrix object with given dimensions.
	 * 
	 *  @param rows number of rows
	 *  @param columns number of columns
	 */
	public Matrix(int rows, int columns) {
		this.rows = rows;
		this.columns = columns;
		
		data = new double[rows][columns];
	}
	
	/**
	 * Constructor.
	 * Creates a new matrix object from given two-dimensional double array.
	 * 
	 *  @param data matrix data as a two-dimensional double array
	 */
	public Matrix(double[][] data) {
		this.data = data.clone();
		this.rows = data.length;
		this.columns = data[0].length;
	}
	
	
	/**
	 * Constructor.
	 * Creates a new matrix object from given one-dimensional double array.
	 * 
	 *  @param data matrix data as a one-dimensional double array
	 */
	public Matrix(double[] data) {
		
		this.rows = data.length;
		this.columns = 1;
		
		this.data = new double[rows][columns];
		
		for (int row = 0; row < rows; row++) {
			this.data[row][0] = data[row];
		}
		
	}
	
	
	/**
	 * Prints out a text representation of the matrix
	 */
	public void print() {
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				System.out.print(data[row][column] + " ");
			}
			System.out.println();
		}
	}
	
	
	/**
	 * Enters data into matrix using given scanner.
	 * 
	 * @param scanner scanner used to get input data
	 */
	public void enterValues(Scanner scanner) {
		
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				System.out.print("Enter Value for data[" + row + "][" + column + "]: ");
				data[row][column] = scanner.nextDouble();
			}
		}
	}
	
	
	/**
	 * Randomizes matrix data with random values between lower and upper limit.
	 * 
	 * @param lowerLimit lower limit
	 * @param upperLimit upper limit
	 */
	public void randomize(double lowerLimit, double upperLimit) {

		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				data[row][column] = Math.random() * (upperLimit - lowerLimit) + lowerLimit;
			}
		}
	}
	
	
	/**
	 * Randomizes matrix data with values between 0.0 and 1.0.
	 */
	public void randomize() {
		
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				data[row][column] = Math.random();
			}
		}
	}

	
	/**
	 * Initializes matrix data with scalar.
	 * 
	 * @param scalar value to initialize every element of the matrix with.
	 */
	public void initializeWithScalar(double scalar) {

		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				data[row][column] = scalar;
			}
		}
		
	}
	
	
	/**
	 * Adds a scalar value to every data point inside the matrix.
	 * 
	 * @param scalar scalar used for the addition
	 */
	public void add(double scalar) {
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				data[row][column] += scalar;
			}
		}
	}
	
	
	/**
	 * Adds a given matrix to the matrix.
	 * 
	 * @param matrix matrix used for the addition. Has to have the same dimensions.
	 */
	public void add(Matrix matrix) {
		
		if (this.rows == matrix.rows && this.columns == matrix.columns) {
			for (int row = 0; row < rows; row++) {
				for (int column = 0; column < columns; column++) {
					data[row][column] += matrix.data[row][column];
				}
			}
		}
	}
	
	
	/**
	 * Multiplies a scalar value to every data point inside the matrix.
	 * 
	 * @param scalar scalar used for the multiplication.
	 */
	public void multiply(double scalar) {
		
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				data[row][column] *= scalar;
			}
		}
	}
	
	
	/**
	 * Multiplies every component of the matrix with the components of a given matrix at the same coordinates.
	 * 
	 * @param matrix matrix used for the multiplication. Has to have the same dimensions.
	 */	
	public void multiplyComponents(Matrix matrix) {
		
		if (this.rows == matrix.rows && this.columns == matrix.columns) {
			for (int row = 0; row < rows; row++) {
				for (int column = 0; column < columns; column++) {
					data[row][column] *= matrix.data[row][column];
				}
			}
		}
	}
	
	
	/**
	 * Calculates matrix product with given matrix and returns the result.
	 * 
	 * @param matrix matrix to be multiplied with our matrix. Has to have the same row count, as this.matrix's number of columns. 
	 * @return matrix product
	 */
	public Matrix multiply(Matrix matrix) {
		
		Matrix result = null;
		
		// check for correct dimensions
		// this.columns must match matrix.rows.
		if (this.columns == matrix.rows) {
			
			Matrix a = this;
			Matrix b = matrix;
			
			result = new Matrix(a.rows, b.columns);
			
			for (int row = 0; row < result.rows; row++) {
				for (int column = 0; column < result.columns; column++) {

					// dot product here
					double sum = 0.0;
					
					for (int i = 0; i < a.columns; i++) {
						sum += a.data[row][i] * b.data[i][column];
					}
					result.data[row][column] = sum;
				}
			}
		}
		return result;
	}
	
	
	/**
	 * Calculates matrix product of given matrices and returns the result.
	 * 
	 * @param a first matrix. a.columns has to be the same as b.rows.
	 * @param b second matrix. b.rows has to be the same as a.columns.
	 * @return matrix product of a x b
	 */
	static public Matrix multiply(Matrix a, Matrix b) {
		
		Matrix result = null;
		
		// check for correct dimensions
		// a.columns must match b.rows.
		if (a.columns == b.rows) {
			
			result = new Matrix(a.rows, b.columns);
			
			for (int row = 0; row < result.rows; row++) {
				for (int column = 0; column < result.columns; column++) {

					// dot product here
					double sum = 0.0;
					
					for (int i = 0; i < a.columns; i++) {
						sum += a.data[row][i] * b.data[i][column];
					}
					result.data[row][column] = sum;
				}
			}
		}
		
		return result;
	}
	
	
	/**
	 * Transposes the matrix (rows and columns are swapped).
	 */
	public void transpose() {
		Matrix result = new Matrix(this.columns, this.rows);
		
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				result.data[column][row] = data[row][column];
			}
		}
		
		this.rows = result.rows;
		this.columns = result.columns;
		this.data = result.data;
	}
	
	
	/**
	 * Returns a double array, representing the flattened matrix.
	 * 
	 * @return double array, representing the flattend matrix.
	 */
	public double[] toArray() {
		double[] array = new double[this.rows * this.columns];
		
		int arrayIndex = 0;
		
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				array[arrayIndex++] = data[row][column];
			}
		}
		
		return array;
	}
	
	
	// TODO: Find a better place to do this.
	//       Implmenet something like matrix.map(function()) to have this generalized.
	//       For now this works for our purposes, though.
	/*
	 * Apply the sigmoid function to every matrix element.
	 */
	public void applySigmoidFunction() {
		
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				data[row][column] = Activation.sigmoid(data[row][column]);
			}
		}
	}
	
}
