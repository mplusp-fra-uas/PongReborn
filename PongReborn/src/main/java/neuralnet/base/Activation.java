package neuralnet.base;


/**
 * Class containing activation functions/methods to be used by perceptrons/neurons.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class Activation {

	/**
	 * Takes a double as an argument and returns a double in the range of 0.0 to 1.0.
	 * Basically very large negative input values become 0.0 and very large positive input values
	 * become 1.0.
	 * 
	 * @param x the input value
	 * @return double value between 0.0 and 1.0 depending on input.
	 */
	public static double sigmoid(double x) {
		return 1 / (1 + Math.exp(-x));
	}
}
