package neuralnet.base;

import neuralnet.utility.Matrix;


/**
 * A class representing a fully connected feed forward neural network.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class NeuralNetwork {

	private int numberOfInputNodes;
	private int numberOfHiddenNodes;
	private int numberOfOutputNodes;
	
	private Matrix weightsHiddenToInputs;
	private Matrix weightsOutputsToHidden;
	
	private Matrix biasHidden;
	private Matrix biasOutput;
	
	
	/**
	 * Constructor.
	 * 
	 * @param numberOfInputNodes number of inputs nodes
	 * @param numberOfHiddenNodes number of hidden nodes
	 * @param numberOfOutputNodes number of output nodes
	 */
	public NeuralNetwork(int numberOfInputNodes, int numberOfHiddenNodes, int numberOfOutputNodes) {
		
		// save neural network structure data
		this.numberOfInputNodes = numberOfInputNodes;
		this.numberOfHiddenNodes = numberOfHiddenNodes;
		this.numberOfOutputNodes = numberOfOutputNodes;
		
		// generate weights matrices and fill it with random data
		this.weightsHiddenToInputs = new Matrix(numberOfHiddenNodes, numberOfInputNodes);
		this.weightsOutputsToHidden = new Matrix(numberOfOutputNodes, numberOfHiddenNodes);
		weightsHiddenToInputs.randomize(-10, 10);
		weightsOutputsToHidden.randomize(-10, 10);
		
		// generate biases
		biasHidden = new Matrix(numberOfHiddenNodes, 1);
		biasOutput = new Matrix(numberOfOutputNodes, 1);
		biasHidden.randomize(-5, 5);
		biasOutput.randomize(-5, 5);
	}
	

	/**
	 * Process inputs and return outputs.
	 * 
	 * @param inputs the input values
	 * @return the generated outputs
	 */
	public double[] feedforward(double[] inputs) {
		
		// convert inputs array to matrix
		Matrix inputsMatrix = new Matrix(inputs);
//		inputsMatrix.print();

		// calculate hidden layer
		Matrix hiddenLayer = Matrix.multiply(weightsHiddenToInputs, inputsMatrix);		
		hiddenLayer.add(biasHidden);
		hiddenLayer.applySigmoidFunction();
		
		// calculate output layer
		Matrix outputLayer = Matrix.multiply(weightsOutputsToHidden, hiddenLayer);
		outputLayer.add(biasOutput);
		outputLayer.applySigmoidFunction();
		
		return outputLayer.toArray();
	}
	
	// TODO: implement this
	/**
	 * Save neural network to a file.
	 * 
	 * @param fileName Name of the file to save to.
	 */
	public void save(String fileName) {
		
	}
	
	// TODO: implement this
	/**
	 * Load neural network from a file.
	 * 
	 * @param fileName Name of the file to read from.
	 */
	public void load(String fileName) {
		
	}
	
	
	/**
	 * Prints out NeuralNetwork.
	 */
	public void print() {
		System.out.println("numberOfInputNodes = " + numberOfInputNodes);
		System.out.println("numberOfHiddenNodes = " + numberOfHiddenNodes);
		System.out.println("numberOfOutputNodes = " + numberOfOutputNodes);
	}

	
	/**
	 * Prints out NeuralNetwork with more details.
	 */
	public void printVerbose() {
		System.out.println("\n--------");
		
		System.out.println("input, hidden, output: " + numberOfInputNodes + ", " + numberOfHiddenNodes + ", " + numberOfOutputNodes);
		
		System.out.println("\nweightsBetweenInputAndHidden: ");
		weightsHiddenToInputs.print();
		
		System.out.println("\nweightsBetweenHiddenAndOutput: ");
		weightsOutputsToHidden.print();
		
		System.out.println("\nbiasHidden: ");
		biasHidden.print();
		
		System.out.println("\nbiasOutput: ");
		biasOutput.print();
		
		System.out.println("--------\n");
	}
	

	/**
	 * Outputs the fields representing the NeuralNetwork as a string.
	 * 
	 * @return the fields representing the NeuralNetwork as a string.
	 */
	@Override
	public String toString() {
		return "{ " + numberOfInputNodes + ", " + numberOfHiddenNodes + ", " + numberOfOutputNodes + " }";
	}
}
