package neuralnet.base;

import java.util.Random;

import neuralnet.utility.Utility;


/**
 * A class representing a perceptron.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class Perceptron {

	private int numberOfInputs;
	private double[] weights;
	private double bias;


	/**
	 * Constructor to set certain weights and bias.
	 * 
	 * @param numberOfInputs number of inputs
	 * @param weights the input weights
	 * @param bias the bias
	 */
	public Perceptron(int numberOfInputs, double[] weights, double bias) {
		this.numberOfInputs = numberOfInputs;
		this.weights = weights;
		this.bias = 0.0;
	}
	
	
	/**
	 * Constructor that generates random weights and bias.
	 * 
	 * @param numberOfInputs number of inputs
	 */
	public Perceptron(int numberOfInputs) {
		this.numberOfInputs = numberOfInputs;
		this.weights = new double[numberOfInputs];
		
		Random random = new Random();
		setRandomWeights();
		this.bias = random.nextGaussian();
	}


	/**
	 * Calculates the weighted sum of all inputs. 
	 * 
	 * @param inputs inputs input values
	 * @return the weighted sum of all inputs.
	 */
	private double calculateWeightedSum(double[] inputs) {
		
		double weightedSum = 0.0;

		for (int i = 0; i < numberOfInputs; i++) {
			System.out.print(weightedSum + " += " + inputs[i] + " * " + weights[i] + " = ");
			weightedSum += inputs[i] * weights[i];
			System.out.println(weightedSum);
		}
		
		weightedSum += bias;
		
		return weightedSum;
	}
	
	
	/**
	 * Sets weight to new random values. 
	 */
	private void setRandomWeights() {

		Random random = new Random();
		
		for (int i = 0; i < numberOfInputs; i++) {
			weights[i] = random.nextGaussian();
		}
	}

	
	/**
	 * Processes inputs and returns output 
	 * 
	 * @param inputs input values
	 * @return the activation
	 */
	public double feedforward(double[] inputs) {

		double weightedSum = calculateWeightedSum(inputs);
		double activation = Activation.sigmoid(weightedSum);

		return activation;
	}


	/**
	 * Prints out perceptron to console
	 */
	public void print() {
		System.out.println("numberOfInputs = " + numberOfInputs);
		System.out.println("weights = " + Utility.toString(weights));
		System.out.println("bias = " + bias);
	}


	/**
	 * Outputs the fields representing the object as a string.
	 * 
	 * @return the fields representing the object as a string.
	 */
	@Override
	public String toString() {
		return "{ " + numberOfInputs + ", " + Utility.toString(weights) + ", " + bias + " }";
	}
}
