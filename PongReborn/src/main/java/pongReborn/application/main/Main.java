package pongReborn.application.main;
	
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;
import pongReborn.application.screens.AiScreen;
import pongReborn.application.screens.GameScreen;
import pongReborn.application.screens.StartScreen;
import pongReborn.application.screens.StatisticScreen;
import pongReborn.application.screens.manager.SceneManager;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;


/**
 * Sets up the main window and adds all screens to the SceneManager
 * 
 * @author Hendrik Mueller
 *
 */
public class Main extends Application {
	
	static Stage primaryStage = null;
	
	static int width = 1000;
	static int height = 700;
	
	StartScreen startScreen = new StartScreen();
	GameScreen gameScreen = new GameScreen();;
	AiScreen aiscreen = new AiScreen();
	StatisticScreen statScreen = new StatisticScreen();
	
	VBox startRoot = startScreen.getScreen();
	VBox aiRoot = aiscreen.getScreen();
	VBox gameRoot = gameScreen.getGameScreen(width, height);
	VBox statRoot = statScreen.getScreen();
	
	Scene startScene = new Scene(startRoot, width, height);
	
	/**
	 * sets up the application screen and add all screens to the SceneManager
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			Main.primaryStage = primaryStage;
			
			primaryStage.setScene(startScene);
			primaryStage.setTitle("PinkPong");
			primaryStage.setResizable(false);
			primaryStage.show();
			
			//center the primary stage on the users monitor
			Rectangle2D primaryBounds = Screen.getPrimary().getVisualBounds();
			primaryStage.setX((primaryBounds.getWidth() - primaryStage.getWidth()) / 2);
			primaryStage.setY((primaryBounds.getHeight() - primaryStage.getHeight()) / 2);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		//create a SceneManager and fill it with roots and primaryScene to switch between them in other classes
		SceneManager sceneManager = new SceneManager();
		sceneManager.setPrimaryScene(startScene);
		sceneManager.addRoot("start", startRoot);
		sceneManager.addRoot("game", gameRoot);
		sceneManager.addRoot("ai", aiRoot);
		sceneManager.addRoot("stat", statRoot);
		
		// TODO: better way to do this?
		sceneManager.setStartScreen(startScreen);
		sceneManager.setGameScreen(gameScreen);
		
	}
	
	/**
	 * Main function launches the application
	 * 
	 * @param args start arguments of the main function
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	/**
	 * Getter for primaryStage
	 * 
	 * @return primaryStage get the PrimaryStage of the application
	 */
	public static Stage getPrimaryStage() {
		return primaryStage;
	}
	
	/**
	 * Getter for width
	 * 
	 * @return width width of the application window
	 */
	public static int getWidth() {
		return width;
	}

	/**
	 * Setter for width
	 * 
	 * @param width width of the application window
	 */
	public static void setWidth(int width) {
		Main.width = width;
	}

	/**
	 * Getter for height
	 * 
	 * @return height height of the application window
	 */
	public static int getHeight() {
		return height;
	}

	/**
	 * Setter for height
	 * 
	 * @param height new height for the application window
	 */
	public static void setHeight(int height) {
		Main.height = height;
	}
}
