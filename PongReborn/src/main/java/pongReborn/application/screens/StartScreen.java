package pongReborn.application.screens;

import java.io.InputStream;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import pongReborn.application.ai.AiAgent;
import pongReborn.application.ai.GeneticAlgorithmAgent;
import pongReborn.application.ai.UnbeatableAgent;
import pongReborn.application.screens.manager.SceneManager;
import pongReborn.application.util.PinkButton;

import java.util.ArrayList;
import java.util.List;

/**
 * class defines the appearance of the startScreen 
 * 
 * @author Hendrik Mueller
 *
 */
public class StartScreen {
	
	public static final double defaultKeyFrameDuration = 8.3;
	public static final double minKeyFrameDuration = 1;
	public static final double maxKeyFrameDuration = 100;
	public static final double keyFrameDurationStepSize = 1;

	private Timeline timeline;
	private double keyFrameDuration = defaultKeyFrameDuration;

	/**
	 * creates a predefined VBox which represents the startScreen
	 * 
	 * @return VBox
	 */
	public VBox getScreen() {

		//add a VBox, which gets returned later as the screen
		VBox box = new VBox();
		box.setAlignment(Pos.CENTER);
		box.setSpacing(20);
		box.setBackground(Background.EMPTY);
		String style = "-fx-background-color: rgba(0,0,0 ,1 )";
		box.setStyle(style);

		//add elements
		Label pongTitle = new Label();
		Label pinkTitle = new Label();

		//configure elements
		//Title "PinkPong"
		pongTitle.setText("Pong");
		pongTitle.setAlignment(Pos.CENTER);
		pongTitle.setFont(new Font(90));
		pongTitle.setTextFill(Color.WHITE);

		pinkTitle.setText("Pink");
		pinkTitle.setAlignment(Pos.CENTER);
		pinkTitle.setFont(new Font(90));
		pinkTitle.setTextFill(Color.LIGHTPINK);

		//create buttons
		//Start Game
		PinkButton startPinkButton = new PinkButton(300, 50, "Start Game", Color.WHITE, Color.BLACK, Color.WHITE, 2, 40); 
		StackPane startPinkButtonPane = startPinkButton.getPinkButton();

		//AIs
		PinkButton aiPinkButton = new PinkButton(300, 50, "AIs", Color.WHITE, Color.BLACK, Color.WHITE, 2, 40); 
		StackPane aiPinkButtonPane = aiPinkButton.getPinkButton();

		//Statistics
		PinkButton statPinkButton = new PinkButton(300, 50, "Statistics", Color.WHITE, Color.BLACK, Color.WHITE, 2, 40); 
		StackPane statPinkButtonPane = statPinkButton.getPinkButton();

		//Quit
		PinkButton quitPinkButton = new PinkButton(300, 50, "Quit", Color.WHITE, Color.BLACK, Color.WHITE, 2, 40); 
		StackPane quitPinkButtonPane = quitPinkButton.getPinkButton();

		//create pinkPantherImage
		Class<?> clazz = StartScreen.class;
		ImageView pinkPanther = new ImageView();
		InputStream input = clazz.getResourceAsStream("/pongReborn/application/util/pinkpongpanther-black.png");
		Image pinkImage = new Image(input);
		pinkPanther.setImage(pinkImage);
		pinkPanther.setScaleX(0.7);
		pinkPanther.setScaleY(0.7);

		//create HBox for PinkPong title
		HBox pinkPong = new HBox();
		pinkPong.getChildren().add(pinkTitle);
		pinkPong.getChildren().add(pongTitle);
		pinkPong.setAlignment(Pos.CENTER);

		//events
		startPinkButtonPane.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent c) {
				startGameWithDefaultAgents();
			}
		});

		AiScreen aiScreen = new AiScreen();
		aiPinkButtonPane.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				SceneManager.getSceneManager().switchRoot("ai");
				aiScreen.loadAgentNames();
			}

		});

		statPinkButtonPane.setOnMouseClicked(e -> SceneManager.getSceneManager().switchRoot("stat"));
		quitPinkButtonPane.setOnMouseClicked(e -> Platform.exit());

		startPinkButtonPane.setOnMouseEntered(e -> { 
			startPinkButton.setTextColor(Color.WHITE);
			startPinkButton.setButtonBackground(Color.LIGHTPINK);
		});

		startPinkButtonPane.setOnMouseExited(e -> {
			startPinkButton.setTextColor(Color.WHITE);
			startPinkButton.setButtonBackground(Color.BLACK);
		});

		aiPinkButtonPane.setOnMouseEntered(e -> {
			aiPinkButton.setTextColor(Color.WHITE);
			aiPinkButton.setButtonBackground(Color.LIGHTPINK);
		});

		aiPinkButtonPane.setOnMouseExited(e -> {
			aiPinkButton.setTextColor(Color.WHITE);
			aiPinkButton.setButtonBackground(Color.BLACK);
		});

		statPinkButtonPane.setOnMouseEntered(e -> {
			statPinkButton.setTextColor(Color.WHITE);
			statPinkButton.setButtonBackground(Color.LIGHTPINK);
		});

		statPinkButtonPane.setOnMouseExited(e -> {
			statPinkButton.setTextColor(Color.WHITE);
			statPinkButton.setButtonBackground(Color.BLACK);
		});

		quitPinkButtonPane.setOnMouseEntered(e -> {
			quitPinkButton.setTextColor(Color.WHITE);
			quitPinkButton.setButtonBackground(Color.LIGHTPINK);
		});

		quitPinkButtonPane.setOnMouseExited(e -> {
			quitPinkButton.setTextColor(Color.WHITE);
			quitPinkButton.setButtonBackground(Color.BLACK);
		});

		//create gridPane
		GridPane grid = new GridPane();
		grid.setHgap(20);
		grid.setVgap(20);
		grid.setAlignment(Pos.CENTER);

		//adding title and buttons to the grid
		grid.add(pinkPong, 0, 0);
		grid.add(startPinkButtonPane, 0, 1);
		grid.add(aiPinkButtonPane, 0, 2);
		grid.add(statPinkButtonPane, 0, 3);
		grid.add(quitPinkButtonPane, 0, 4);

		//create and fill hBox
		HBox content = new HBox();
		content.getChildren().add(pinkPanther);
		content.getChildren().add(grid);
		content.setAlignment(Pos.CENTER);

		//add elements to vBox -> the actual screen
		box.getChildren().add(content);

		return box;
	}

	// TODO: comment and implement following
	
	public void createTimeline(double keyFrameDuration) {
		if (timeline != null) {
			timeline.stop();
		}
		this.keyFrameDuration = keyFrameDuration;
		timeline = new Timeline(new KeyFrame(Duration.millis(keyFrameDuration), e -> SceneManager.getSceneManager().getGameScreen().runGame(SceneManager.getSceneManager().getGameScreen().getCanvas().getGraphicsContext2D())));
		timeline.setCycleCount(Timeline.INDEFINITE);
		timeline.play();
	}

	
	public void startGame() {
		createTimeline(defaultKeyFrameDuration);
		SceneManager.getSceneManager().switchRoot("game");
		SceneManager.getSceneManager().getGameScreen().setupControls();
	}


	public void startGameWithDefaultAgents() {
		createTimeline(defaultKeyFrameDuration);
		SceneManager.getSceneManager().switchRoot("game");
		SceneManager.getSceneManager().getGameScreen().setupControls();

		List<AiAgent> aiAgents = new ArrayList<AiAgent>();
		aiAgents.add(new GeneticAlgorithmAgent("Darwin (Genetic)"));
		aiAgents.add(new UnbeatableAgent("Fluffy (Unbeatable)"));
		SceneManager.getSceneManager().getGameScreen().setupAiAgents(aiAgents);
	}


	/**
	 * Getter for timeline
	 * 
	 * @return Timeline which is used for the animation created in GameScreen
	 */
	public Timeline getTimeline() {
		return timeline;
	}

	
	/**
	 * @return the keyFrameDuration
	 */
	public double getKeyFrameDuration() {
		return keyFrameDuration;
	}
}
