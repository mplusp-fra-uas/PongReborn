package pongReborn.application.screens.manager;

import java.util.HashMap;

import javafx.scene.Parent;
import javafx.scene.Scene;
import pongReborn.application.screens.AiScreen;
import pongReborn.application.screens.GameScreen;
import pongReborn.application.screens.StartScreen;

/**
 * class is used to switch between different screens in the application
 * 
 * @author Hendrik Mueller
 */
public class SceneManager {
	
	private static SceneManager sceneManager;
	private static Scene primaryScene = null;
	private static HashMap<String, Parent> rootMap = new HashMap<>();
	
	public static GameScreen gameScreen;
	public static StartScreen startScreen;
	public static AiScreen aiScreen;
	
	/**
	 * adds a Screen to the memory of the SceneManager
	 * 
	 * @param key		string which identifies the Screen 
	 * @param root		root element which represents a screen
	 */
	public void addRoot(String key, Parent root) {
		rootMap.put(key, root);
	}
	
	/**
	 * switches to a root saved in the memory of the SceneManager
	 * 
	 * @param key		string which identifies the Screen which should be switched to
	 */
	public void switchRoot(String key) {
		primaryScene.setRoot(rootMap.get(key));;
	}
	
	/**
	 * set primaryScene which should be used to switch roots
	 * 
	 * @param primary primary scene of selected primaryStage
	 */
	public void setPrimaryScene(Scene primary) {
		SceneManager.primaryScene = primary;
	}
	
	/**
	 * Getter for rootMap
	 * 
	 * @return rootMap HashMap of the screens key and the screen itself as root
	 */
	public static HashMap<String, Parent> getHashMap() {
		return rootMap;
	}
	
	/**
	 * Getter for primaryScene
	 * 
	 * @return primaryScene Scene which is used by the SceneManager for switching rootss
	 */
	public static Scene getPrimaryScene() {
		return primaryScene;
	}
	
	/**
	 * Getter for sceneManager
	 * 
	 * @return sceneManager SceneManager returns the active instance 
	 */
	public static SceneManager getSceneManager() {
		if (sceneManager != null) {
			return sceneManager;
		} else {
			sceneManager = new SceneManager();
			return sceneManager;
		}
	}

	
	// TODO: comment this if there is no better way to do this
	public GameScreen getGameScreen() {
		return gameScreen;
	}

	public void setGameScreen(GameScreen gameScreen) {
		SceneManager.gameScreen = gameScreen;
	}
	
	public StartScreen getStartScreen() {
		return startScreen;
	}

	public void setStartScreen(StartScreen startScreen) {
		SceneManager.startScreen = startScreen;
	}
	
	public AiScreen getAiScreen() {
		return aiScreen;
	}

	public void setStartScreen(AiScreen aiScreen) {
		SceneManager.aiScreen = aiScreen;
	}


}
