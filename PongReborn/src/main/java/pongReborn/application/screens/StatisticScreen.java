package pongReborn.application.screens;

import java.io.InputStream;

import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import pongReborn.application.main.Main;
import pongReborn.application.screens.manager.SceneManager;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * class defines the appearance of the StatisticScreen
 * 
 * @author Hendrik Mueller
 *
 */
public class StatisticScreen {
	
	/**
	 * Getter for box
	 * 
	 * @return box a VBox which contains the elements of the StatisticScreen
	 */
	public VBox getScreen() {
		//add a VBox 
		VBox box = new VBox();
		box.setAlignment(Pos.CENTER);
		box.setSpacing(20);
		box.getStyleClass().addAll("pane", "vbox");
		box.setId("statBox");
		box.getStylesheets().add(getClass().getResource("screenDesign.css").toString());
		
		//add axes for xyChart
		final NumberAxis datasets = new NumberAxis();
        final NumberAxis followingHits = new NumberAxis();
		
		//add elements
        //add image for chartNode
      		Class<?> clazz = StartScreen.class;
      		ImageView pinkPantherNode = new ImageView();
      		InputStream input = clazz.getResourceAsStream("/pongReborn/application/util/pinkpongpanther-black.png");
      		Image pinkImageNode = new Image(input);
      		pinkPantherNode.setImage(pinkImageNode);
		
      	LineChart<Number,Number> lineChart = new LineChart<Number,Number>(datasets, followingHits);
		
		ComboBox<String> agentSelection = new ComboBox<String>();
		Label selectAgent = new Label();
		Button backButton = new Button();
		
		
		//set properties of elements
		datasets.setLabel("number of datasets");
		followingHits.setLabel("number of following hits");
		datasets.setAutoRanging(false);
		followingHits.setAutoRanging(false);
		datasets.setTickUnit(10);
		followingHits.setTickUnit(10);
		
		lineChart.setTitle("Learning curve of selected agent");
		lineChart.setCreateSymbols(false);
		lineChart.setLegendVisible(false);
		lineChart.setPrefSize(((Main.getWidth()/3)*2)-50, ((Main.getHeight()/3)*2)-50);
		
		
		agentSelection.setPrefSize((Main.getWidth()/3)-50, 50);
		
		selectAgent.setPrefSize((Main.getWidth()/3)-50, 50);
		selectAgent.setText("Select Agent: ");
		selectAgent.setAlignment(Pos.CENTER);
		selectAgent.setFont(new Font(32));
		selectAgent.setTextFill(Color.WHITE);
		
		backButton.setPrefSize((Main.getWidth()/3) - 50, 50);
		backButton.setAlignment(Pos.CENTER);
		backButton.setText("Back");
		backButton.setFont(new Font(32));
		
		//define a series for the chart
		XYChart.Series<Number, Number> testSeries = new Series<Number, Number>();
        testSeries.setName("test series");
        
        //fill series with data
        //TODO: get data for the series from the selected agents datasets
        
        testSeries.getData().add(new Data<Number, Number>(0, 10));
        testSeries.getData().add(new Data<Number, Number>(40, 39));
        testSeries.getData().add(new Data<Number, Number>(70, 67));
        testSeries.getData().add(new Data<Number, Number>(90, 98));
        
        lineChart.getData().add(testSeries);
        
        StackPane chartPane = new StackPane();
        chartPane.setAlignment(Pos.CENTER_RIGHT);
        chartPane.getChildren().add(lineChart);
        
        StackPane agentSelectPane = new StackPane();
        agentSelectPane.setAlignment(Pos.CENTER_LEFT);
        agentSelectPane.getChildren().add(agentSelection);
        
        VBox selectBox = new VBox();
		selectBox.setAlignment(Pos.CENTER);
		selectBox.getChildren().add(selectAgent);
		selectBox.getChildren().add(agentSelectPane);
		selectBox.getChildren().add(backButton);
		selectBox.setSpacing(30);
        
        GridPane statGrid = new GridPane();
        statGrid.setHgap(20);
        statGrid.setVgap(20);
        statGrid.setAlignment(Pos.CENTER);
        
        statGrid.add(chartPane, 1, 0);
        statGrid.add(selectBox, 0, 0);
        
        //add events
		backButton.setOnMouseClicked(e -> SceneManager.getSceneManager().switchRoot("start"));
        
        //add elements to box
        box.getChildren().add(statGrid);
		
		return box;
	}

}
