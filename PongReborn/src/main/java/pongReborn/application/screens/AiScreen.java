package pongReborn.application.screens;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import pongReborn.application.ai.AiAgent;
import pongReborn.application.ai.BackpropagationAlgorithm;
import pongReborn.application.ai.GeneticAlgorithmAgent;
import pongReborn.application.ai.UnbeatableAgent;
import pongReborn.application.main.Main;
import pongReborn.application.screens.manager.SceneManager;
import pongReborn.application.util.PinkButton;


/**
 * The game class defines the appearance of the AiScreen.
 * 
 * @author Hendrik Mueller
 * @author Marco Peluso
 * @version 1.0
 */
public class AiScreen {
	
	// constants
	private static final String AI_ALGORITHM_UNBEATABLE = "UnbeatableAgent";
	private static final String AI_ALGORITHM_GENETIC = "GeneticAlgorithmAgent";
	private static final String AI_ALGORITHM_BACKPROPAGATION = "BackpropagationAlgorithm";
	
	
	private int height = Main.getHeight();
	private int width = Main.getWidth();
	
	private TextField aiAgentNameTextField = new TextField();
	
	private PinkButton saveButton = new PinkButton(320, 50, "Save", Color.WHITE, Color.BLACK, Color.WHITE, 2, 32);
	private PinkButton backButton = new PinkButton(320, 50, "Back", Color.WHITE, Color.BLACK, Color.WHITE, 2, 32);
	private PinkButton startButton = new PinkButton(320, 50, "Start Game", Color.WHITE, Color.BLACK, Color.WHITE, 2, 32);
	
	private StackPane saveButtonPane = saveButton.getPinkButton();
	private StackPane backButtonPane = backButton.getPinkButton();
	private StackPane startButtonPane = startButton.getPinkButton();
	
	private Label firstAgentNameLabel = new Label();
	private Label secondAgentNameLabel = new Label();
	private Label createAgentLabel = new Label();
	private Label chooseAgentsLabel = new Label();
	private Label chooseAlgorithmLabel = new Label();

	private File allAgents = new File("allAgents.txt");
	private PrintWriter agentWriter;
	
	private static ObservableList<String> availableAiAgentList = FXCollections.observableArrayList();
	private static ObservableList<String> availableAlgorithmList = FXCollections.observableArrayList();
	
	private ComboBox<String> chooseAgent1ComboBox = new ComboBox<String>(availableAiAgentList);
	private static ComboBox<String> chooseAgent2ComboBox = new ComboBox<String>(availableAiAgentList);
	private static ComboBox<String> chooseAlgorithmComboBox = new ComboBox<String>(availableAlgorithmList);
	
	// for access from GameScreen
	private List<AiAgent> availableAiAgents = new ArrayList<AiAgent>();
	private List<AiAgent> chosenAiAgents = new ArrayList<AiAgent>();

	/**
	 * saves the name of the created agent to an external textfile so that we can load all created agents when we start the game at a later point
	 * @param agentName name of the new agent which was created
	 */
	private void saveNewAgentName(String agentName) {
		try {
			agentWriter = new PrintWriter(allAgents, "UTF-8");
			Scanner scanner = new Scanner(allAgents);
			int i = 0;
			
			if(!availableAiAgentList.isEmpty()) {
				for(i = 0; i < availableAiAgentList.size(); i++) {
					agentWriter.println(availableAiAgentList.get(i));
				}
				agentWriter.println(agentName);
				
			} else {
				agentWriter.println(agentName);
			}
			
			
			agentWriter.close();
			scanner.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			System.out.println("Error while trying to write to allAgents.txt");
			e.printStackTrace();
		} 
	}
	
	/**
	 * load all names of agents which were ever created within this application
	 */
	public void loadAgentNames() {
		availableAiAgentList.clear();
		try {
			Scanner scanner = new Scanner(allAgents);
			while(scanner.hasNextLine()) {
				availableAiAgentList.add(scanner.nextLine());
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error while loading the names of all agents from allAgents.txt");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Getter for chosenAiAgents
	 * 
	 * @return List containing selected AiAgents
	 */
	public List<AiAgent> getChosenAiAgents() {
		return chosenAiAgents;
	}
	
	/**
	 * Getter for aiAgents
	 * 
	 * @return List containing all available AiAgents
	 */
	public List<AiAgent> getAiAgents() {
		return availableAiAgents;
	}
	
	
	/**
	 * Getter for box
	 * 
	 * @return box the VBox which contains all elements of the AiScreen
	 */
	public VBox getScreen() {
		
		//add a VBox
		VBox box = new VBox();
		box.setPrefSize(width/2, height/2);
		box.setAlignment(Pos.CENTER);
		box.setSpacing(20);
		box.setBackground(Background.EMPTY);
		String style = "-fx-background-color: rgba(0,0,0 ,1 )";
		box.setStyle(style);
		
		//set properties of elements
		//configure textfield
		aiAgentNameTextField.setAlignment(Pos.CENTER);
		aiAgentNameTextField.setPrefSize(300, 50);
		aiAgentNameTextField.setMaxSize(500, 50);
		aiAgentNameTextField.setStyle("-fx-background-color: lightpink; -fx-text-fill: white");
		aiAgentNameTextField.setFont(new Font(29));
		aiAgentNameTextField.getStylesheets().add(getClass().getResource("screenDesign.css").toString());
		
		chooseAgent1ComboBox.setPrefSize(320, 50);
		chooseAgent1ComboBox.getStylesheets().add(getClass().getResource("screenDesign.css").toString());
		
		chooseAgent2ComboBox.setPrefSize(320, 50);
		chooseAgent2ComboBox.getStylesheets().add(getClass().getResource("screenDesign.css").toString());
		
		chooseAlgorithmComboBox.setPrefSize(320, 50);
		chooseAlgorithmComboBox.getStylesheets().add(getClass().getResource("screenDesign.css").toString());
		
		StackPane firstSpane = new StackPane();
		firstSpane.setAlignment(Pos.CENTER_LEFT);
		firstSpane.getChildren().add(chooseAgent1ComboBox);
		
		StackPane secondSpane = new StackPane();
		secondSpane.setAlignment(Pos.CENTER_LEFT);
		secondSpane.getChildren().add(chooseAgent2ComboBox);
		
		StackPane chooseAiLevel = new StackPane();
		chooseAiLevel.setAlignment(Pos.CENTER_LEFT);
		chooseAiLevel.getChildren().add(chooseAlgorithmComboBox);
		
		firstAgentNameLabel.setPrefSize(320, 50);
		firstAgentNameLabel.setText("Agent 1:");
		firstAgentNameLabel.setFont(new Font(30));
		firstAgentNameLabel.setTextFill(Color.WHITE);
		firstAgentNameLabel.setAlignment(Pos.CENTER);
		
		secondAgentNameLabel.setPrefSize(320, 50);
		secondAgentNameLabel.setText("Agent 2:");
		secondAgentNameLabel.setFont(new Font(30));
		secondAgentNameLabel.setTextFill(Color.WHITE);
		secondAgentNameLabel.setAlignment(Pos.CENTER);
		
		createAgentLabel.setPrefSize(640, 50);
		createAgentLabel.setText("Create Agent:");
		createAgentLabel.setFont(new Font(40));
		createAgentLabel.setTextFill(Color.WHITE);
		createAgentLabel.setAlignment(Pos.CENTER);
	
		chooseAlgorithmLabel.setPrefSize(320, 50);
		chooseAlgorithmLabel.setText("Algorithm:");
		chooseAlgorithmLabel.setFont(new Font(30));
		chooseAlgorithmLabel.setTextFill(Color.WHITE);
		chooseAlgorithmLabel.setAlignment(Pos.CENTER);
		
		chooseAgentsLabel.setPrefSize(640, 50);
		chooseAgentsLabel.setText("Choose Agents:");
		chooseAgentsLabel.setFont(new Font(40));
		chooseAgentsLabel.setTextFill(Color.WHITE);
		chooseAgentsLabel.setAlignment(Pos.CENTER);
		
		GridPane gridpane = new GridPane();
		gridpane.setHgap(20);
		gridpane.setVgap(20);
		//gridpane.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE); //Test
		
		gridpane.add(createAgentLabel, 0, 0, 2, 1);
		gridpane.add(chooseAlgorithmLabel, 0, 1);
		gridpane.add(chooseAiLevel, 1, 1);
		gridpane.add(aiAgentNameTextField, 0, 2);
		gridpane.add(saveButtonPane, 1, 2);
		// gridpane.add(pick, 1, 1);
		gridpane.add(chooseAgentsLabel, 0, 3, 2, 1);
		GridPane.setMargin(chooseAgentsLabel, new Insets(90, 0, 0, 0)); //top, right, bottom, left
		
		gridpane.add(firstAgentNameLabel, 0, 4);
		gridpane.add(secondAgentNameLabel, 1, 4);
		gridpane.add(firstSpane, 0, 5);
		gridpane.add(secondSpane, 1, 5);
		gridpane.add(startButtonPane, 0, 6);
		gridpane.add(backButtonPane, 1, 6);
		
		gridpane.setAlignment(Pos.CENTER);
		
		//add button behavior
		backButtonPane.setOnMouseClicked(e -> SceneManager.getSceneManager().switchRoot("start"));
		
		saveButtonPane.setOnMouseClicked(e -> {
			// TODO: check if algorithm is selected before doing this!
			if(chooseAlgorithmComboBox.getValue() != null) {
				createAiAgent();
			}
			loadAgentNames();
		});
		
		startButtonPane.setOnMouseClicked(e -> {
			// set up chosen agents
			String agent1 = chooseAgent1ComboBox.getValue();
			String agent2 = chooseAgent2ComboBox.getValue();
			if(agent1.contains("(Unbeatable)")) {
				availableAiAgents.add(new UnbeatableAgent(agent1));
			}  
			if(agent1.contains("(Genetic)")) {
				availableAiAgents.add(new GeneticAlgorithmAgent(agent1));
			}  
			if(agent1.contains("(Backpropagation)")) {
				availableAiAgents.add(new BackpropagationAlgorithm(agent2));
			}  
			if(agent2.contains("(Unbeatable)")) {
				availableAiAgents.add(new UnbeatableAgent(agent2));
			}  
			if(agent2.contains("(Genetic)")) {
				availableAiAgents.add(new GeneticAlgorithmAgent(agent2));
			}  
			if(agent2.contains("(Backpropagation)")) {
				availableAiAgents.add(new BackpropagationAlgorithm(agent2));
			}
			
			chosenAiAgents.add(availableAiAgents.get(0));
			chosenAiAgents.add(availableAiAgents.get(1));
			SceneManager.getSceneManager().getGameScreen().setupAiAgents(chosenAiAgents);
			SceneManager.getSceneManager().getStartScreen().startGame();
		});
		
		//add hover effect for the buttons
		backButtonPane.setOnMouseEntered(e -> backButton.setTextColor(Color.LIGHTPINK));
		backButtonPane.setOnMouseExited(e -> backButton.setTextColor(Color.WHITE));
		
		saveButtonPane.setOnMouseEntered(e -> saveButton.setTextColor(Color.LIGHTPINK));
		saveButtonPane.setOnMouseExited(e -> saveButton.setTextColor(Color.WHITE));
		
		startButtonPane.setOnMouseEntered(e -> startButton.setTextColor(Color.LIGHTPINK));
		startButtonPane.setOnMouseExited(e -> startButton.setTextColor(Color.WHITE));
		
		//TODO: add listener for startGameButton

		//add elements to box
		box.getChildren().add(gridpane);
		
		
		// TODO: temporarily add algorithms to list here
		availableAlgorithmList.add(AI_ALGORITHM_UNBEATABLE);
		availableAlgorithmList.add(AI_ALGORITHM_GENETIC);
		availableAlgorithmList.add(AI_ALGORITHM_BACKPROPAGATION);

		return box;
	}
	
	/**
	 * creates an ai agent according to its machine learning algorithm
	 */
	private void createAiAgent() {
		String chosenAlgorithm = chooseAlgorithmComboBox.getValue();
		switch (chosenAlgorithm) {
		case AI_ALGORITHM_UNBEATABLE:
			createUnbeatableAgent();
			break;
		case AI_ALGORITHM_GENETIC:
			createGeneticAlgorithmAgent();
			break;
		case AI_ALGORITHM_BACKPROPAGATION:
			createBackpropagationAlgorithmAgent();
			break;

		default:
			break;
		}
	}
	
	/**
	 * creates an ai agent which is unbeatable, because his x and y positions get constantly updated with the ones of the ball
	 */
	private void createUnbeatableAgent() {
		
		String aiAgentName = aiAgentNameTextField.getText();
		//availableAiAgentList.add(aiAgentName + " (Unbeatable)");
		saveNewAgentName(aiAgentName + " (Unbeatable)");
		
		//availableAiAgents.add(new UnbeatableAgent(aiAgentName));
		//numberOfAvailableAiAgents++;
		
		aiAgentNameTextField.clear();
	}
	
	/**
	 * creates an ai agent which uses the Genetic AlgorithmAgent
	 */
	private void createGeneticAlgorithmAgent() {
		
		String aiAgentName = aiAgentNameTextField.getText();
		//availableAiAgentList.add(aiAgentName + " (Genetic)");
		saveNewAgentName(aiAgentName + " (Genetic)");
		
		//availableAiAgents.add(new GeneticAlgorithmAgent(aiAgentName));
		//numberOfAvailableAiAgents++;
		
		aiAgentNameTextField.clear();
	}
	
	/**
	 * creates an ai agent which uses the Backpropagation algorithm
	 */
	private void createBackpropagationAlgorithmAgent() {
		
		String aiAgentName = aiAgentNameTextField.getText();
		//availableAiAgentList.add(aiAgentName + " (Backpropagation)");
		saveNewAgentName(aiAgentName +" (Backpropagation)");
		
		//availableAiAgents.add(new BackpropagationAlgorithm(aiAgentName));
		//numberOfAvailableAiAgents++;
		
		aiAgentNameTextField.clear();
	}
	
}
