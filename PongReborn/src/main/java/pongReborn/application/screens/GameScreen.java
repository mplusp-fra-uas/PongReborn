package pongReborn.application.screens;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import pongReborn.application.screens.manager.SceneManager;
import pongReborn.application.gamemodels.Game;
import pongReborn.application.ai.AiAgent;
import pongReborn.application.gamemodels.Ball;
import pongReborn.application.gamemodels.Player;
import pongReborn.application.main.Main;

/**
 * This class is the middle man between the players and the game logic.
 * It is forwarding input to the game class and drawing the game.
 * 
 * @author Hendrik Mueller
 * @author Marco Peluso
 * @version 1.0
 */
public class GameScreen {
	
	// class constants
	public static final int PLAYER_NONE_ID = Game.PLAYER_NONE_ID;
	public static final int PLAYER_1_ID = Game.PLAYER_1_ID;
	public static final int PLAYER_2_ID = Game.PLAYER_2_ID;
	
	Canvas canvas;
	
	private Game game = new Game();
	private Player[] players = { game.getPlayer1(), game.getPlayer2() };
	
	private int[] gameStatus = { 0, 0, 0 };
	
	private List<AiAgent> aiAgents = new ArrayList<AiAgent>();
	private boolean[] enabledAiAgents;
	private boolean gameIsPaused = false;

	
	// TODO: comment following
	// TODO: refactor this to be cleaner / more flexible
	public void setupAiAgents(List<AiAgent> aiAgents) {
		
		int i = 0;
		for (AiAgent aiAgent : aiAgents) {
			aiAgent.setGameScreen(this);
			aiAgent.setPlayerId(i);
			i++;
		}

		this.aiAgents = aiAgents;
		this.enabledAiAgents = new boolean[] { true, true };
	}
	
	
	/**
	 * Sets up human player and game controls.
	 */
	public void setupControls() {
		
		// start movement
		SceneManager.getPrimaryScene().setOnKeyPressed(e -> {
			if (e.getCode() == KeyCode.W) {
				setPlayerDirection(PLAYER_1_ID, Player.UP);
			} else if (e.getCode() == KeyCode.D) { 
				setPlayerDirection(PLAYER_1_ID, Player.DOWN);
			} else if (e.getCode() == KeyCode.O) {
				setPlayerDirection(PLAYER_2_ID, Player.UP);
			} else if (e.getCode() == KeyCode.K) {
				setPlayerDirection(PLAYER_2_ID, Player.DOWN);
			}
		});
		
		// stop movement
		SceneManager.getPrimaryScene().setOnKeyReleased(e -> {
			if (e.getCode() == KeyCode.W && players[PLAYER_1_ID].getDirection() == Player.UP) {
				setPlayerDirection(PLAYER_1_ID, Player.STOP);
			} else if (e.getCode() == KeyCode.D && players[PLAYER_1_ID].getDirection() == Player.DOWN) {
				setPlayerDirection(PLAYER_1_ID, Player.STOP);
			} else if (e.getCode() == KeyCode.O && players[PLAYER_2_ID].getDirection() == Player.UP) {
				setPlayerDirection(PLAYER_2_ID, Player.STOP);
			} else if (e.getCode() == KeyCode.K && players[PLAYER_2_ID].getDirection() == Player.DOWN) {
				setPlayerDirection(PLAYER_2_ID, Player.STOP);
			} else if (e.getCode() == KeyCode.ESCAPE) {
				backToStartScreen();
			} else if (e.getCode() == KeyCode.DIGIT1) {
				togglePlayerControl(PLAYER_1_ID);
			} else if (e.getCode() == KeyCode.DIGIT2) {
				togglePlayerControl(PLAYER_2_ID);
			} else if (e.getCode() == KeyCode.DIGIT8) {
				slowDownGame();
			} else if (e.getCode() == KeyCode.DIGIT9) {
				speedUpGame();
			} else if (e.getCode() == KeyCode.DIGIT0) {
				resetGameSpeed();
			} else if (e.getCode() == KeyCode.SPACE) {
				togglePauseGame();
			}
		});
	}
	

	/**
	 * Makes game slower.
	 */
	private void slowDownGame() {
		double currentKeyFrameDuration = SceneManager.getSceneManager().getStartScreen().getKeyFrameDuration();
		double keyFrameDurationStepSize = StartScreen.keyFrameDurationStepSize;
		
		if (currentKeyFrameDuration + keyFrameDurationStepSize <= StartScreen.maxKeyFrameDuration) {
			SceneManager.getSceneManager().getStartScreen().createTimeline(currentKeyFrameDuration + keyFrameDurationStepSize);
		}
	}
	
	
	/**
	 * Makes game faster.
	 */
	private void speedUpGame() {
		double currentKeyFrameDuration = SceneManager.getSceneManager().getStartScreen().getKeyFrameDuration();
		double keyFrameDurationStepSize = StartScreen.keyFrameDurationStepSize;
		
		if (currentKeyFrameDuration - keyFrameDurationStepSize >= StartScreen.minKeyFrameDuration) {
			SceneManager.getSceneManager().getStartScreen().createTimeline(currentKeyFrameDuration - keyFrameDurationStepSize);
		}
	}
	
	
	/**
	 * Resets game to default speed.
	 */
	private void resetGameSpeed() {
		SceneManager.getSceneManager().getStartScreen().createTimeline(StartScreen.defaultKeyFrameDuration);
	}

	
	/**
	 * Pauses the game.
	 */
	private void pauseGame() {
		SceneManager.getSceneManager().getStartScreen().getTimeline().stop();
		gameIsPaused = true;
	}
	
	
	/**
	 * Unpauses the game.
	 */
	private void unPauseGame() {
		SceneManager.getSceneManager().getStartScreen().createTimeline(SceneManager.getSceneManager().getStartScreen().getKeyFrameDuration());
		gameIsPaused = false;
	}
	
	
	/**
	 * Pauses the game.
	 */
	private void togglePauseGame() {
		if (gameIsPaused) {
			unPauseGame();
		} else {
			pauseGame();
		}
	}
	
	
	/**
	 * Stops current game and goes back to start screen.
	 */
	private void backToStartScreen() {
		SceneManager.getSceneManager().switchRoot("start");
		SceneManager.getSceneManager().getStartScreen().getTimeline().stop();
		resetGame();
	}
	
	
	/**
	 * Sets movement direction of a player with matching playerId.
	 * 
	 * @param playerId player id
	 * @param direction player movement direction
	 */
	public void setPlayerDirection(int playerId, int direction) {
		players[playerId].setDirection(direction);
	}
	
	
	// TODO: handle, if no AiAgent exists for playerId
	/**
	 * Toggles between human and AiAgent controlling player with matching playerId.
	 * 
	 * @param playerId player id
	 */
	public void togglePlayerControl(int playerId) {
		enabledAiAgents[playerId] = !enabledAiAgents[playerId];
		setPlayerDirection(playerId, Player.STOP);
	}
	
	
	/**
	 * Resets game object.
	 */
	private void resetGame() {
		game.setPlayerScores(new int[] { 0, 0 });
		
		//reset paddle position
		game.getPlayer1().setYPos(Main.getHeight()/2);
		game.getPlayer2().setYPos(Main.getHeight()/2);
		
		//reset ball position
		game.getBall().setXPos(Main.getWidth()/2);
		game.getBall().setYPos(Main.getHeight()/2);
		
		// reset player directions
		setPlayerDirection(PLAYER_1_ID, Player.STOP);
		setPlayerDirection(PLAYER_2_ID, Player.STOP);
	}
	

	/**
	 * returns the gamescreen as a VBox
	 * 
	 * @param width of the window
	 * @param height of the window
	 * @return returns a VBox which includes the whole gamescreen
	 */
	public VBox getGameScreen(int width, int height) {
		
		//add a VBox
		VBox box = new VBox();

		//add a Canvas
		canvas = new Canvas(width, height);

		//add canvas to box
		box.getChildren().add(canvas);

		return box;
	}

	// TODO: comment methods without comments
	
	private void updateAgents() {
		
		updateAgentsWithPositions();
		updateAgentsWithScores();
	}
	
	
	private void updateAgentsWithPositions() {
		
		// only update enabled agents
		int i = 0;
		for (AiAgent aiAgent : aiAgents) {
			if (enabledAiAgents[i] == true) {
				aiAgent.updateInputs(players[i].getYPos(), game.getBall().getYPos());
			}
			i++;
		}
	}
	
	
	private void updateAgentsWithScores() {
		
		// only update enabled agents
		int i = 0;
		for (AiAgent aiAgent : aiAgents) {
			if (enabledAiAgents[i] == true) {
				aiAgent.updatePlayerScores(Arrays.copyOfRange(gameStatus, 0, 2));
			}
			i++;
		}
	}
	
	
	private void increasePlayerHitCount(int playerId) {
		if ((playerId == PLAYER_1_ID || playerId == PLAYER_2_ID) && enabledAiAgents[playerId] == true) {
			if (aiAgents.get(playerId) != null) {
				aiAgents.get(playerId).increaseHitCount();
			}
		}
	}
	
	
	/**
	 * runs the pong game through detecting collisions and moving the paddles
	 * 
	 * @param gc graphicscontext of the chosen canvas
	 */
	public void runGame(GraphicsContext gc) {
		
		gameStatus = game.run();
		int collisionPlayerId = gameStatus[2];
		increasePlayerHitCount(collisionPlayerId);
		updateAgents();
		drawGameFrame(gc);
	}

	
	/**
	 * draws a black background to the canvas of gc
	 * 
	 * @param gc graphicscontext of the given canvas
	 */
	public void drawBackground(GraphicsContext gc) {
		gc.setFill(Color.BLACK);
		gc.fillRect(0, 0, game.getWidth(), game.getHeight());
	}
	
	
	/**
	 * draws the first paddle/player to the canvas of gc
	 * 
	 * @param gc graphicscontext of the given canvas
	 */
	public void drawPlayer1(GraphicsContext gc) {
		gc.setFill(Color.WHITE);
		gc.fillRect(players[PLAYER_1_ID].getXPos(), players[PLAYER_1_ID].getYPos(), players[PLAYER_1_ID].getWidth(), players[PLAYER_1_ID].getHeight());
	}
	
	
	/**
	 * draws the second paddle/player to the canvas of gc
	 * 
	 * @param gc graphicscontext of the given canvas
	 */
	public void drawPlayer2(GraphicsContext gc) {
		gc.setFill(Color.WHITE);
		gc.fillRect(players[PLAYER_2_ID].getXPos(), players[PLAYER_2_ID].getYPos(), players[PLAYER_2_ID].getWidth(), players[PLAYER_2_ID].getHeight());
	}
	
	
	/**
	 * draws the ball to the canvas of gc
	 * 
	 * @param gc graphicscontext of the given canvas
	 */
	public void drawBall(GraphicsContext gc) {
		Ball ball = game.getBall();
		gc.setFill(Color.LIGHTPINK);
		gc.fillOval(ball.getXPos(), ball.getYPos(), ball.getDiameter(), ball.getDiameter());
//		gc.fillRect(ball.getXPos(), ball.getYPos(), ball.getDiameter(), ball.getDiameter());
	}
	
	
	/**
	 * draws the scoreboard of the game to the canvas of gc
	 * 
	 * @param gc graphicscontext of the given canvas
	 */
	public void drawScoreBoard(GraphicsContext gc) {
		gc.setFill(Color.LIGHTPINK);
		gc.setFont(new Font(40));
		gc.fillText(gameStatus[PLAYER_1_ID] + "", (game.getWidth() / 2) / 2, 100);
		gc.fillText(gameStatus[PLAYER_2_ID] + "", game.getWidth() - (game.getWidth() / 4), 100);
		
		gc.setFill(Color.WHITE);
		gc.setTextAlign(TextAlignment.CENTER);
		if (aiAgents.get(PLAYER_1_ID) != null) {
			gc.fillText(aiAgents.get(PLAYER_1_ID).getName(), (game.getWidth() / 2) / 2, 40);
		}
		if (aiAgents.get(PLAYER_2_ID) != null) {
			gc.fillText(aiAgents.get(PLAYER_2_ID).getName(), game.getWidth() - (game.getWidth() / 4), 40);
		}
	}
	
	
	/**
	 * Draws a dashed vertical line in the middle of the playing field.
	 * 
	 * @param gc graphicscontext of the given canvas
	 */
	public void drawDashedCenterline(GraphicsContext gc) {
		
		gc.setFill(Color.LIGHTPINK);
		
		int lineWidth = 10;
		int gameHeight = game.getHeight();
		int segmentHeight = gameHeight / 25;
		int numberOfSegments = gameHeight / segmentHeight;
		
		int lineStartXPos = game.getWidth() / 2 - lineWidth / 2;
		int lineStartYPos = 0;
		
		for (int i = 0; i < numberOfSegments; i++) {
			if (i % 2 == 0) {
				gc.fillRect(lineStartXPos, lineStartYPos + (i * segmentHeight), lineWidth, segmentHeight);
			}
		}
	}
	
	
	/**
	 * draws the complete gameframe including background, players/paddles, ball and scoreboard
	 * 
	 * @param gc graphicscontext of the given canvas
	 */
	public void drawGameFrame(GraphicsContext gc) {
		drawBackground(gc);
		drawDashedCenterline(gc);
		drawPlayer1(gc);
		drawPlayer2(gc);
		drawBall(gc);
		drawScoreBoard(gc);
	}

	
	/**
	 * Getter for canvas
	 * 
	 * @return returns the canvas of the GameScreen object 
	 */
	public Canvas getCanvas() {
		return canvas;
	}

	
	/**
	 * Getter for game.
	 * 
	 * @return returns the game object 
	 */
	public Game getGame() {
		return game;
	}
}
