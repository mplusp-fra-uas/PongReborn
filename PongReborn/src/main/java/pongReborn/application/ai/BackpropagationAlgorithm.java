/**
 * @author Steffen Hanzlik
 * @author Marco Peluso 
 */

package pongReborn.application.ai;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.MomentumBackpropagation;
import org.neuroph.util.TrainingSetImport;
import org.neuroph.util.TransferFunctionType;

import pongReborn.application.gamemodels.Ball;
import pongReborn.application.gamemodels.Game;
import pongReborn.application.gamemodels.Player;
import pongReborn.application.screens.GameScreen;


public class BackpropagationAlgorithm implements AiAgent {
	NeuralNetwork neuralNet;
	DataSet dataSet;
	DataSetRow dataRow;
	MomentumBackpropagation learningRule;
	
	BufferedWriter writer;
	
//	default values
	
	private int maxHitStreaks;
	private int currentHitStreaks;
	
	public int generation =0;
	
	private String file = "data.txt";
	
	public boolean fromFileEnabled = false;
	
	public boolean started = false;
	
	// player
	private int playerId;
	private int[] playerScores;
	
	//gamescreen
	private GameScreen gameScreen;
	private String name;
	
	//Layers
	private int inputs = 2;
	private int outputs=1;
	private int hiddens=5;
	
	//Inputs
	
	int input_1; // ball y position
	int input_2; // player y position
	double desOutput;
	double output;
	
	private int numberOfDataSets;
	
	//Calculated output beeing put in an array
	
	private double [] outputArray;
	
	/**
	 * Initialise the Backpropagation algorithm
	 * 
	 * @param game the game object
	 * @param ball the ball object
	 */
	
	public BackpropagationAlgorithm(Game game, Ball ball) {
		neuralNet = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, inputs, hiddens, outputs);
		learningRule= (MomentumBackpropagation) neuralNet.getLearningRule();
		
		learningRule.setLearningRate(0.2);
		learningRule.setMomentum(0.9);
		learningRule.setMaxError(0.01);
		
		dataSet = new DataSet(inputs, outputs);
		
		playerScores = new int[] { 0, 0 };
		
		TrainingSet trainingSet = new TrainingSet(name);

	}
	
	
	public double getDesOutput() {
		return desOutput = getNeuralNetworkOutput(); 
	}
	
	public void startNN() {
		writeToFile(file);
		if(fromFileEnabled) {
			loadDataFromFile(file);
		}else {
			createDataSet();
			fromFileEnabled = true;
		}
	}
	
	/**
	 * Destructor of the DataSet if you want a new one
	 */
	
	public void clearDataSet() {
		dataSet = new DataSet (inputs,outputs);
	}
	
	/**
	 * If there is no DataSet there have to be created a new one which can be teached
	 */
	
	public void createDataSet() {
		System.out.println("Creating DataSet... ");
		

		dataSet.addRow(new DataSetRow(new double[] {getInput_1 (), getInput_2()}, new double [] {getDesOutput()}));
		
		System.out.println(dataSet);
		System.out.println("Data Created!" );
		
		teachNeuralNetwork();
	}
	

	/**
	 * neural Network learn from his Data Set
	 */
	
	public void teachNeuralNetwork() {
		System.out.println("Learning... ");
		
		neuralNet.learn(dataSet);
		
		System.out.println("Data learnt!");
		
		generation++;
		
		calculateOutput(); 
	}
	
	/**
	 * Calculate output from input layers over the hidden layer
	 */
	public void calculateOutput() {
		System.out.println("Output getting calculated...  ");
		
		System.out.print(dataSet);
		
		for(int i=0; i< dataSet.size(); i++) {
	 
			dataRow = dataSet.getRowAt(i);
			neuralNet.setInput(dataRow.getInput());
			neuralNet.calculate();
			
			outputArray= neuralNet.getOutput();
			output= outputArray[0];
			
			System.out.println(i);
			playGame(output);
			if(!fromFileEnabled) {
				clearDataSet();
			}
		}
		System.out.println("Output calculated!");
		if(fromFileEnabled) {
			clearDataSet();
			fromFileEnabled = false;
		}
	}
	
	/**
	 * 
	 * @param output says the agent which part he had to do
	 */
	
	public void playGame(double output) {
		
		if (output >= 0.55) {
			gameScreen.setPlayerDirection(playerId, Player.UP);
		} else if (output <= 0.45) {
			gameScreen.setPlayerDirection(playerId, Player.DOWN);
		} else {
			gameScreen.setPlayerDirection(playerId, Player.STOP);
		}
		

	}
	
	// Dont know if it works. maybe get it from genetic Algorithm
	
	public void loadDataFromFile(String file) {
		fromFileEnabled =true;
		System.out.print("Loading dataSet from File... ");
		try {
			dataSet = TrainingSetImport.importFromFile(file, inputs, outputs,",");
			System.out.println(dataSet);
		}catch (NumberFormatException | IOException ex) {
			System.out.println(ex);
		}
		System.out.println("Finished loading dataSet from file!");
		teachNeuralNetwork();
	}
	
    public void writeToFile(String filename) {       
        try {
            writer = new BufferedWriter(new FileWriter(filename, true));
            writer.write(getInput_1() + "," + getInput_2() + "," + getDesOutput());
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            System.out.println(e);
        }
        System.out.println("Written one line!");
} 
	/**
	 * Constructor.
	 * 
	 * @param name name of the agent
	 */
	public BackpropagationAlgorithm(String name) {
		this.name = name;
		playerScores = new int[] { 0, 0 };
		dataSet = new DataSet(inputs, outputs);
		
		neuralNet = new MultiLayerPerceptron(TransferFunctionType.SIGMOID, inputs, hiddens, outputs);
		learningRule= (MomentumBackpropagation) neuralNet.getLearningRule();
		
		learningRule.setLearningRate(0.2);
		learningRule.setMomentum(0.9);
		learningRule.setMaxError(0.01);
//		TrainingSet trainingSet = new TrainingSet(name);
	}
	/**
	 * Constructor setting up default values.
	 */
	public BackpropagationAlgorithm() {
		this("The Oracle");
	}
	/**
	 * 
	 * @return NeuralNetworks Output
	 */
	public double getNeuralNetworkOutput() {
		return output;
	}
	
	/**
	 * 
	 * @return input1 is the BallYPos
	 */
	
	public double getInput_1(){
		if(input_1 < 0) {
			input_1=0;
		}
		return input_1;
	}
/**
 * 
 * @return input2 is the PlayerYPos
 */
	public int getInput_2() {
		if(input_2 <0) {
			input_2 =0;
		}
		return input_2;
	}

	public int getGeneration() {
		return generation;
	}
	/**
	 * Sets the AiAgent's name.
	 * 
	 * @param name the AiAgent's name
	 */
	@Override
	public void setName(String name) {
		this.name = name;
		
	}
	/**
	 * Returns the AiAgent's name.
	 * 
	 * @return the AiAgent's name
	 */
	@Override
	public String getName() {
		return name;
	}
	/**
	 * The AiAgent has to make a decision based on its own position and the position of the ball. 
	 */
	@Override
	public void updateInputs(int agentYPosition, int ballYPosition) {
		if (!started) {
			startNN();
		}
		
		input_1 = ballYPosition;
		input_2 = agentYPosition;
		
		double guess = getNeuralNetworkOutput();
		
		if (guess >= 0.55) {
			gameScreen.setPlayerDirection(playerId, Player.UP);
		} else if (guess <= 0.45) {
			gameScreen.setPlayerDirection(playerId, Player.DOWN);
		} else {
			gameScreen.setPlayerDirection(playerId, Player.STOP);
		}
		
	}
	/**
	 * We need the GameScreen to interact with the game.
	 * That's where we set it.
	 * 
	 * @param gameScreen the GameScreen we interact with
	 */
	@Override
	public void setGameScreen(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
		
	}
	/**
	 * Sets the AiAgent's playerId.
	 * 
	 * @param playerId the agents player id
	 */
	@Override
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
		
	}
	/**
	 * Returns the AiAgent's playerId.
	 * 
	 * @return the AiAgent's player id
	 */
	@Override
	public int getPlayerId() {
		return playerId;
	}
	/**
	 * Returns the AiAgent's number of generated data sets.
	 * 
	 * @return the AiAgent's number of generated data sets
	 */
	@Override
	public int getNumberOfDataSets() {
		return numberOfDataSets;
	}
	/**
	 * Returns the AiAgent's max hit streak.
	 * 
	 * @return the AiAgent's max hit streak.
	 */
	@Override
	public int getMaxHitStreak() {
		return maxHitStreaks;
	}
	/**
	 * Notify AiAgent about it successful hitting the ball.
	 */
	@Override
	public void increaseHitCount() {
		System.out.println("increaseHitCount playerId " + playerId);
		currentHitStreaks++;
		if (currentHitStreaks > maxHitStreaks) {
			maxHitStreaks = currentHitStreaks;
		}
	}

	
	/**
	 * Update the AiAgent when the game's score changes.
	 * 
	 * @param playerScores int array with player scores
	 */
	@Override
	public void updatePlayerScores(int[] playerScores) {

		// if the other player scores
		if ( (this.playerScores[GameScreen.PLAYER_2_ID] < playerScores[GameScreen.PLAYER_2_ID] && this.playerId == GameScreen.PLAYER_1_ID) ||
			 (this.playerScores[GameScreen.PLAYER_1_ID] < playerScores[GameScreen.PLAYER_1_ID] && this.playerId == GameScreen.PLAYER_2_ID)) {
			// we failed!
			System.out.println("We failed!");
		}
		this.playerScores = playerScores;
	}

	
}


