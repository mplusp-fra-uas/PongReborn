package pongReborn.application.ai;


import neuralnet.base.NeuralNetwork;


/**
 * A class representing a genome for the GeneticAlgorithmAgent.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class Genome {


	// TODO: make this flexible. NeuralNetwork has to be upgraded for that
	private static final int numberOfLayers = 3;

	private int[] layerSizes;

	private NeuralNetwork neuralNetwork;
	private int fitness;

	private double[] genomeSequence;
	private int genomeSequenceLength;


	/**
	 * Constructor.
	 * 
	 * @param numberOfInputNodes number of inputs nodes
	 * @param numberOfHiddenNodes number of hidden nodes
	 * @param numberOfOutputNodes number of output nodes
	 */
	public Genome(int numberOfInputNodes, int numberOfHiddenNodes, int numberOfOutputNodes) {

		neuralNetwork = new NeuralNetwork(numberOfInputNodes, numberOfHiddenNodes, numberOfOutputNodes);
		fitness = 0;
		layerSizes = new int[] { numberOfInputNodes, numberOfHiddenNodes, numberOfOutputNodes };
	}


	/**
	 * Process given inputs and return outputs.
	 * 
	 * @param inputs given inputs
	 * @return given outputs
	 */
	public double[] processInputs(double[] inputs) {
		return neuralNetwork.feedforward(inputs);
	}


	/**
	 * Creates a new genome sequence for the current genome obect.
	 * 
	 */
	public void createGenomeSequence() {
		// genomeSequence format:
		// {
		//    numberOfLayers,  // just 1 value
		//    layerSizes,      // as many as numberOfLayers
		//    biases,          // input/output neurons don't have biases.
		//                     // so we have sum(layerSizes(excluding first and last)) biases
		//    weights          // first layer doesn't have weights, so starting from second layer
		//                     // each layer has previousLayerSize * layerSize weights
		//  }
		genomeSequence = new double[genomeSequenceLength];

		// for biases. input/output layers don't have biases
		for (int i = 1; i < numberOfLayers - 1; i++) {
			genomeSequence[i] = layerSizes[i];
		}

		// for weights. input layer doesn't have weights
		for (int i = 1; i < numberOfLayers; i++) {
			genomeSequence[i] += layerSizes[i - 1] * layerSizes[i];
		}

		System.out.println("genomeSequence = " + genomeSequence);
	}


	/**
	 * Getter for fitness.
	 * 
	 * @return genome's fitness
	 */
	public int getFitness() {
		return fitness;
	}


	/**
	 * Setter for fitness.
	 * 
	 * @param fitness genome's fitness
	 */
	public void setFitness(int fitness) {
		this.fitness = fitness;
		System.out.println("NEW FITNESS: " + fitness);
	}

}
