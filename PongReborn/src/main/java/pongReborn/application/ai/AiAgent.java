package pongReborn.application.ai;


import pongReborn.application.screens.GameScreen;

/**
 * This interface is the API to interact with all kinds of AiAgents.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public interface AiAgent {
	
	/**
	 * Sets the AiAgent's name.
	 * 
	 * @param name the AiAgent's name
	 */
	public void setName(String name);
	
	
	/**
	 * Returns the AiAgent's name.
	 * 
	 * @return the AiAgent's name
	 */
	public String getName();
	
	
	/**
	 * The AiAgent has to make a decision based on its own position and the position of the ball.
	 * 
	 * @param agentYPosition the AiAgent's position
	 * @param ballYPosition the ball's position
	 */
	public void updateInputs(int agentYPosition, int ballYPosition);

	
	/**
	 * Update the AiAgent when the game's score changes.
	 * 
	 * @param playerScores int array with player scores
	 */
	public void updatePlayerScores(int[] playerScores);
	
	
	/**
	 * We need the GameScreen to interact with the game.
	 * That's where we set it.
	 * 
	 * @param gameScreen the GameScreen we interact with
	 */
	public void setGameScreen(GameScreen gameScreen);
	
	
	/**
	 * Sets the AiAgent's playerId.
	 * 
	 * @param playerId the agents player id
	 */
	public void setPlayerId(int playerId);
	
	
	/**
	 * Returns the AiAgent's playerId.
	 * 
	 * @return the AiAgent's player id
	 */
	public int getPlayerId();
	
	
	/**
	 * Returns the AiAgent's number of generated data sets.
	 * 
	 * @return the AiAgent's number of generated data sets
	 */
	public int getNumberOfDataSets();
	
	
	/**
	 * Returns the AiAgent's max hit streak.
	 * 
	 * @return the AiAgent's max hit streak.
	 */
	public int getMaxHitStreak();
	
	
	/**
	 * Notify AiAgent about it successful hitting the ball.
	 */
	public void increaseHitCount();
}
