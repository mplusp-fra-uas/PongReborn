/**
 * @author Steffen Hanzlik, Marco Peluso
 */

package pongReborn.application.ai;

import java.io.File;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.nnet.learning.MomentumBackpropagation;

public class TrainingSet {
	
	NeuralNetwork<?> neuralNetwork;
	BackpropagationAlgorithm neuralNet;
	MomentumBackpropagation learningRule;
	DataSet trainingSet;
	DataSetRow trainingSetRow;
	
	
	//properties
	static String aiName = "ai";
	String fileName;
	File file = new File(fileName);
	
	//Layers
	public int inputs= 3;
	public int outputs=1;
	public int hidden_Layers = 5;
	
	public boolean isFirstLine = true;
	
	GeneticAlgorithmAgent genetic;
	
	public TrainingSet(BackpropagationAlgorithm neuralNet) {
		this.neuralNet = neuralNet;
	}
	
	protected boolean fileExists() {
		return file.exists() && !file.isDirectory();
	}
	
	
	public TrainingSet(String name) {
		
		//create training set 
		trainingSet = new DataSet(inputs,outputs);
		aiName = name;
		fileName = name + ".nnet";
	
	
	}
	
//	protected void saveFile() throws FileNotFoundException,UnsupportedEncodingException { 
//		try (PrintWriter writer = new PrintWriter(new FileOutputStream(file,true))){
//			if(! isFirstLine) {
//				writer.print("\n");
//			} else {
//				isFirstLine = false;
//			}
//				
//			for(int i=0; i<genetic.getNumberOfGenomes(); i++) {
//				for(int j=0; j<genetic.getNumberOfHiddenLayers()-1;j++) {
//					for (int k=0; k<genetic.getNumberOfNeurons()[j];k++) {
//						int m=0; 
//						if(j+1!= genetic.getNumberOfHiddenLayers()-1) {
//							m=genetic.getNumberOfNeurons()[j+1] -1;
//						}
//						else {
//							m=genetic.getNumberOfNeurons()[j+1];
//						}
//						
//						for(int l=0; l<m; l++) {
//							writer.print(genetic.getSynapses()[i][j][k][l] + " ");
//						}			
//					}
//				}
//			}
//		}
//	}
		
		
		

	
//	public void loadTrainingSet() throws FileNotFoundException, IOException {
//		String currentLine, lastGen = null;
//		
//		BufferedReader reader = new BufferedReader(new FileReader(file));
//		
//		while((currentLine = reader.readLine()) != null) {
//			lastGen = currentLine;
//		}
//		
//		List<String> values = Arrays.asList(lastGen.trim().split(" "));
//		
//		
//		//Initialise  Synapsis 
//		
//		int h = 0;
//		for(int i = 0; i < genetic.getNumberOfGenomes(); i++) {
//			for(int j = 0; j < genetic.getNumberOfHiddenLayers(); j++) {
//				for(int k = 0; k < genetic.getNumberOfNeurons()[j]; k++) {
//					int n;
//					if(j + 1 != genetic.getNumberOfHiddenLayers() -1) {
//						n = genetic.getNumberOfNeurons()[j + 1] -1;
//					}
//					else {
//						n = genetic.getNumberOfNeurons()[j + 1];
//					}
//					for(int l = 0; l < n; l++) {
//						genetic.getSynapses()[i][j][k][l] = Double.parseDouble(values.get(h));
//						h++;
//					}
//				}
//			}
//		}
//		reader.close();
//	}
}
