package pongReborn.application.ai;

import pongReborn.application.gamemodels.Player;
import pongReborn.application.screens.GameScreen;

/**
 * An ai agent that always stays on the same height as the ball.
 * This agent doesn't have any learning capabilities.
 * It is just hard coded.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class UnbeatableAgent implements AiAgent {

	private String name;
	private int playerId;
	private GameScreen gameScreen;
	
	private int[] playerScores;
	private int currentHitStreak;
	private int maxHitStreak;
	private int numberOfDataSets;

	
	/**
	 * Constructor.
	 * 
	 * @param name name of the agent
	 */
	public UnbeatableAgent(String name) {
		this.name = name;
		playerScores = new int[] { 0, 0 };
		currentHitStreak = 0;
	}

	
	/**
	 * Constructor setting up default values.
	 */
	public UnbeatableAgent() {
		this("The Rock");
	}

	
	/**
	 * Sets the AiAgent's name.
	 * 
	 * @param name the AiAgent's name
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * Returns the AiAgent's name.
	 * 
	 * @return the AiAgent's name
	 */
	@Override
	public String getName() {
		return name;
	}
	
	
	/**
	 * The AiAgent has to make a decision based on its own position and the position of the ball.
	 * 
	 * @param agentYPosition the AiAgent's position
	 * @param ballYPosition the ball's position
	 */
	@Override
	public void updateInputs(int agentYPosition, int ballYPosition) {

		int playerHeight = gameScreen.getGame().getPlayer1().getHeight();
		int ballDiameter = gameScreen.getGame().getBallDiameter();
		
		if (agentYPosition + playerHeight / 2 == ballYPosition + ballDiameter / 2) {
			gameScreen.setPlayerDirection(playerId, Player.STOP);
		} else if (agentYPosition + playerHeight / 2 > ballYPosition + ballDiameter / 2) {
			gameScreen.setPlayerDirection(playerId, Player.UP);
		} else {
			gameScreen.setPlayerDirection(playerId, Player.DOWN);
		}
	}
	
	
	/**
	 * Update the AiAgent when the game's score changes.
	 * 
	 * @param playerScores int array with player scores
	 */
	@Override
	public void updatePlayerScores(int[] playerScores) {
//		this.playerScores = playerScores;
		
		// if the other player scores
		if ( (this.playerScores[GameScreen.PLAYER_2_ID] < playerScores[GameScreen.PLAYER_2_ID] && this.playerId == GameScreen.PLAYER_1_ID) ||
			 (this.playerScores[GameScreen.PLAYER_1_ID] < playerScores[GameScreen.PLAYER_1_ID] && this.playerId == GameScreen.PLAYER_2_ID)) {
			// we failed!
		}
		this.playerScores = playerScores;
	}

	
	/**
	 * We need the GameScreen to interact with the game.
	 * That's where we set it.
	 * 
	 * @param gameScreen the GameScreen we interact with
	 */
	@Override
	public void setGameScreen(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}
	
	
	/**
	 * Sets the AiAgent's playerId.
	 * 
	 * @param playerId the agents player id
	 */
	@Override
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	
	/**
	 * Returns the AiAgent's playerId.
	 * 
	 * @return the AiAgent's player id
	 */
	@Override
	public int getPlayerId() {
		return playerId;
	}


	/**
	 * Returns the AiAgent's number of generated data sets.
	 * 
	 * @return the AiAgent's number of generated data sets
	 */
	@Override
	public int getNumberOfDataSets() {
		return numberOfDataSets;
	}


	/**
	 * Returns the AiAgent's max hit streak.
	 * 
	 * @return the AiAgent's max hit streak.
	 */
	@Override
	public int getMaxHitStreak() {
		return maxHitStreak;
	}


	/**
	 * Notify AiAgent about it successful hitting the ball.
	 */
	@Override
	public void increaseHitCount() {

		currentHitStreak++;
		
		if (currentHitStreak > maxHitStreak) {
			maxHitStreak = currentHitStreak;
		}
		
//		System.out.println(name + " (id:" + playerId + ") currentHitStreak: " + currentHitStreak);
	}
	
}
