package pongReborn.application.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pongReborn.application.gamemodels.Player;
import pongReborn.application.screens.GameScreen;

/**
 * An ai agent using a genetic algorithm to learn and improve itself.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class GeneticAlgorithmAgent implements AiAgent {

	private static final String defaultName = "Darwin";
	
	// parameters
	private static final int numberOfInputNodes = 2;
	private static final int numberOfHiddenNodes = 2;
	private static final int numberOfOutputNodes = 1;
	
	private static final int bestGenomeCount = 2;
	private static final int bestGenomeCrossoverCount = 1;
	private static final int bestGenomeMutateCount = 1;
	private static final int randomGenomeCount = 1;
	
	private static final int numberOfGenomesPerGeneration = bestGenomeCount + bestGenomeCrossoverCount + bestGenomeMutateCount + randomGenomeCount;
	
	// generation stuff
	private List<Genome> currentGeneration;

	private int currentGenerationNumber;
	private int currentGenomeNumber;
	
	private List<Integer> fitnessRankList; // saves fitness scores from every genome in current generation
	
	private int numberOfDataSets;
	
	// game relevant stuff
	private String name;
	private int playerId;
	private GameScreen gameScreen;
	
	private int[] playerScores;
	private int currentHitStreak;
	
	private int maxHitStreak;

	
	// used for controlling logging frequency
	private static int updateInputsCallCounter = 0;
	private static int updateInputsCallCounterDampingFactor = 50;
	

	/**
	 * Constructor. 
	 * 
	 * @param name name of agent
	 */
	public GeneticAlgorithmAgent(String name) {
		
		// agent stuff
		this.name = name;

		// set up first generation
		createFirstGeneration();
		currentGenomeNumber = 0;
		fitnessRankList = new ArrayList<Integer>();

		for(int i = 0; i < numberOfGenomesPerGeneration; i++) {
			fitnessRankList.add(0);
		}
		
		// initialize game data stuff
		playerScores = new int[] { 0, 0 };
		
		currentHitStreak = 0;
	}
	
	
	/**
	 * Constructor. 
	 */
	public GeneticAlgorithmAgent() {
		this(defaultName);
	}

	
	/**
	 * Creates first generation. 
	 */
	private void createFirstGeneration() {
		currentGeneration = createRandomGenomes(numberOfGenomesPerGeneration);
		currentGenerationNumber++;
		System.out.println("Created generation " + currentGenerationNumber);
	}
	
	
	// TODO: tweak/make values adjustable!
	// TODO: implement this
	/**
	 * Creates next generation. 
	 */
	private void createNextGeneration() {
		
		// prepare next generation
		List<Genome> nextGeneration = new ArrayList<Genome>();
		
		// add best genomes
		List<Genome> bestGenomes = getBestGenomes(bestGenomeCount);
		nextGeneration.addAll(bestGenomes);
		
		// add crossovers of best genomes
		List<Genome> crossoversOfBestGenomes = createCrossoverOfBestGenomes(bestGenomeCrossoverCount);
		nextGeneration.addAll(crossoversOfBestGenomes);
		
		// add mutations of best genomes
		List<Genome> mutatedBestGenomes = mutateBestGenomes(bestGenomeMutateCount);
		nextGeneration.addAll(mutatedBestGenomes);
		
		// add random genomes
		List<Genome> randomGenomes = createRandomGenomes(randomGenomeCount);
		nextGeneration.addAll(randomGenomes);
		
		// replace current generation with next generation
		currentGeneration = nextGeneration;
		currentGenerationNumber++;
		
		numberOfDataSets += numberOfGenomesPerGeneration;
		
//		System.out.println("numberOfDataSets: " + numberOfDataSets);
		System.out.println("Created generation " + currentGenerationNumber);
	}
	
	
	/**
	 * Creates random genomes.
	 * 
	 *  @return A list containing random genomes.
	 */
	private List<Genome> createRandomGenomes(int numberOfGenomes) {
	
		List<Genome> randomGenomes = new ArrayList<Genome>();
		
			for (int i = 0; i < numberOfGenomes; i++) {
				randomGenomes.add(new Genome(numberOfInputNodes, numberOfHiddenNodes, numberOfOutputNodes));
			}
			
			return randomGenomes;
	}
	

	// TODO: implement this
	/**
	 * Finds and returns current generation's best genomes.
	 * 
	 *  @return A list containing current generation's best genomes.
	 */
	private List<Genome> getBestGenomes(int numberOfGenomes) {
		
		List<Genome> bestGenomes = new ArrayList<Genome>();
		
		System.out.println("fitnessRankList: " + fitnessRankList);
		
		for (int i = 0; i < numberOfGenomes; i++) {
			
			// find index of max fitness
			int maxFitnessIndex = fitnessRankList.indexOf(Collections.max(fitnessRankList));
			fitnessRankList.set(maxFitnessIndex, -1); // set fitness to -1, so that the index cannot be found again
			
			// add found genome to best genome list
			bestGenomes.add(currentGeneration.get(maxFitnessIndex));
			System.out.println("found maxFitnessIndex: " + maxFitnessIndex);
			
		}
		
		return bestGenomes;

	}
	
	
	// TODO: implement this
	private List<Genome> createCrossoverOfBestGenomes(int numberOfGenomes) {
		
		// TODO: fix this to actual implementation!
		return createRandomGenomes(numberOfGenomes);
	}
	
	
	// TODO: implement this
	private List<Genome> mutateBestGenomes(int numberOfGenomes) {
		
		// TODO: fix this to actual implementation!
		return createRandomGenomes(numberOfGenomes);
	}
	
	
	// TODO: implement this
	// TODO: comment uncommented stuff
	
	public void togglelearningOn() {
		
		// TODO: implement these methods correctly
		// TODO: this should toggle agents mode between learning mode (actually running the algorithm to get better)
		//       and simply running it while using the currently best known genome without getting better.
		
	}
	
	
	// ************************ BEGIN * AiAgent Interface Implementation * BEGIN ************************

	// TODO: implement the following methods
	// TODO: comment the following methods
		
	/**
	 * Sets the AiAgent's name.
	 * 
	 * @param name the AiAgent's name
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * Returns the AiAgent's name.
	 * 
	 * @return the AiAgent's name
	 */
	@Override
	public String getName() {
		return name;
	}


	/**
	 * The AiAgent has to make a decision based on its own position and the position of the ball.
	 * 
	 * @param agentYPosition the AiAgent's position
	 * @param ballYPosition the ball's position
	 */
	@Override
	public void updateInputs(int agentYPosition, int ballYPosition) {
		
		double[] inputs = { agentYPosition, ballYPosition };
		double[] guess = currentGeneration.get(currentGenomeNumber).processInputs(inputs);
		
		if (guess[0] >= 0.55) {
			gameScreen.setPlayerDirection(playerId, Player.UP);
		} else if (guess[0] <= 0.45) {
			gameScreen.setPlayerDirection(playerId, Player.DOWN);
		} else {
			gameScreen.setPlayerDirection(playerId, Player.STOP);
		}
		
//		if (guess[0] >= 0.75) {
//			gameScreen.setPlayerDirection(playerId, Player.UP);
//		} else {
//			gameScreen.setPlayerDirection(playerId, Player.DOWN);
//		}
		
		// prevent too much spam
		updateInputsCallCounter++;
		if (updateInputsCallCounter > updateInputsCallCounterDampingFactor) {
			System.out.println(name + " (id:" + playerId + ") guess: " + guess[0]);
			updateInputsCallCounter = 0;
		}
	}

	
	/**
	 * Update the AiAgent when the game's score changes.
	 * 
	 * @param playerScores int array with player scores
	 */
	@Override
	public void updatePlayerScores(int[] playerScores) {

		if ( (this.playerScores[GameScreen.PLAYER_2_ID] < playerScores[GameScreen.PLAYER_2_ID] && this.playerId == GameScreen.PLAYER_1_ID) ||
			 (this.playerScores[GameScreen.PLAYER_1_ID] < playerScores[GameScreen.PLAYER_1_ID] && this.playerId == GameScreen.PLAYER_2_ID)) {

			// if the other player scores (=> if current genome failed to hit the ball)
			handleEnemyScored();
			System.out.println(name + " (id:" + playerId + ") missed the ball!");
			
		}
		this.playerScores = playerScores;
	}

	
	private void handleEnemyScored() {
		
		int currentGenomeFitness = currentHitStreak;
		
		currentGeneration.get(currentGenomeNumber).setFitness(currentGenomeFitness);
		fitnessRankList.set(currentGenomeNumber, currentGenomeFitness);
		
		System.out.println("currentGeneration.get(currentGenomeNumber).getFitness(): " + currentGeneration.get(currentGenomeNumber).getFitness());
		
		currentHitStreak = 0;
		
		if (currentGenomeNumber < numberOfGenomesPerGeneration - 1 ) {
			currentGenomeNumber++;
		} else {
			createNextGeneration();
			currentGenomeNumber = 0;
		}
	}
	
	/**
	 * We need the GameScreen to interact with the game.
	 * That's where we set it.
	 * 
	 * @param gameScreen the GameScreen we interact with
	 */
	@Override
	public void setGameScreen(GameScreen gameScreen) {
		this.gameScreen = gameScreen;
	}

	
	/**
	 * Sets the AiAgent's playerId.
	 * 
	 * @param playerId the agents player id
	 */
	@Override
	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	
	/**
	 * Returns the AiAgent's playerId.
	 * 
	 * @return the AiAgent's player id
	 */
	@Override
	public int getPlayerId() {
		return playerId;
	}

	
	/**
	 * Returns the AiAgent's number of generated data sets.
	 * 
	 * @return the AiAgent's number of generated data sets
	 */
	@Override
	public int getNumberOfDataSets() {
		return numberOfDataSets;
	}

	
	/**
	 * Returns the AiAgent's max hit streak.
	 * 
	 * @return the AiAgent's max hit streak.
	 */
	@Override
	public int getMaxHitStreak() {
		return maxHitStreak;
	}

	
	/**
	 * Notify AiAgent about it successful hitting the ball.
	 */
	@Override
	public void increaseHitCount() {
		
		currentHitStreak++;
		
		System.out.println(name + " (id:" + playerId + ") currentHitStreak: " + currentHitStreak);
	}
	
	// ************************ END * AiAgent Interface Implementation * END ************************
	
}
