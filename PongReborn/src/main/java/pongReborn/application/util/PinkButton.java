package pongReborn.application.util;

import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Paint;

/**
 * This class is used to create our own designed buttons while simultaneously reducing our code in the Screen classes.
 * 
 * @author Hendrik Mueller
 */

public class PinkButton {
	
	int buttonWidth, buttonHeight, buttonFontsize;
	double buttonStrokeWidth;
	String buttonText;
	Paint buttonTextColor, buttonBackgroundColor, buttonStrokeColor;
	
	Rectangle rect = new Rectangle();
	Label label = new Label();
	StackPane sPane = new StackPane();
	
	/**
	 * Constructor. 
	 * 
	 * @param	width			width of the button  
	 * @param	height			height of the button
	 * @param   text			text displayed on the button
	 * @param	textColor		color of displayed text
	 * @param	buttonColor		background color of the button 
	 * @param	strokeColor		color of the button border
	 * @param	strokeWidth		width of the button border
	 * @param	fontsize		size of the button font
	 * 
	 */
	public PinkButton(int width, int height, String text, Paint textColor, Paint buttonColor, Paint strokeColor, double strokeWidth, int fontsize) {
		buttonWidth = width;
		buttonHeight = height;
		buttonText = text;
		buttonBackgroundColor = buttonColor;
		buttonStrokeColor = strokeColor;
		buttonStrokeWidth = strokeWidth;
		
		buttonFontsize = fontsize;
		buttonTextColor = textColor;
		
		rect.setFill(buttonBackgroundColor);
		rect.setStroke(buttonStrokeColor);
		rect.setStrokeWidth(buttonStrokeWidth);
		rect.setWidth(buttonWidth);
		rect.setHeight(buttonHeight);
		
		label.setPrefSize(buttonWidth, buttonHeight);
		label.setText(buttonText);
		label.setAlignment(Pos.CENTER);
		label.setFont(new Font(buttonFontsize));
		label.setTextFill(buttonTextColor);
		
		sPane.getChildren().add(rect);
		sPane.getChildren().add(label);
		
	}
	
	/**
	 * returns the button
	 * 
	 * @return sPane the button as a StackPane
	 */
	public StackPane getPinkButton() {
		return sPane;
	}
	
	/**
	 * set color of the button text
	 * 
	 * @param color new color for the buttons text
	 */
	public void setTextColor(Paint color) {
		label.setTextFill(color);
	}
	
	/**
	 * set border color of the button
	 * 
	 * @param color new color for the buttons border
	 */
	public void setButtonStrokeColor(Paint color) {
		rect.setStroke(color);
	}
	
	/**
	 * set button background color
	 * 
	 * @param color new color of the buttons background
	 */
	public void setButtonBackground(Paint color) {
		rect.setFill(color);
	}
	
	/**
	 * set width of the button border
	 * 
	 * @param strokeWidth new strokeWidth of the buttons border
	 */
	public void setButtonStrokeWidth(double strokeWidth) {
		rect.setStrokeWidth(strokeWidth);
	}
}
