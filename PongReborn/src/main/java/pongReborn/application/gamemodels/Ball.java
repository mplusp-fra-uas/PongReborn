package pongReborn.application.gamemodels;


/**
 * Represents the ball in the game.
 * 
 * @author Marco Peluso
 * @version 1.0
 */
public class Ball {
	
	private int xPos;
	private int yPos;
	private int diameter;
	
	private int xStepSize;
	private int yStepSize;
	private int xDirection;
	private int yDirection;


	/**
	 * Constructor. Creates a ball using the given paramaters.
	 * 
	 * @param xPos			x position
	 * @param yPos			y position
	 * @param diameter		diameter
	 * @param xStepSize		this is the amount the ball travels on the x axis in one game frame
	 * @param yStepSize		this is the amount the ball travels on the y axis in one game frame
	 * @param xDirection	controls the direction on the x axis
	 * @param yDirection	controls the direction on the y axis
	 */
	public Ball(int xPos, int yPos, int diameter, int xStepSize, int yStepSize, int xDirection, int yDirection) {
		this.xPos = xPos;
		this.yPos = yPos;
		this.diameter = diameter;
		
		this.xStepSize = xStepSize;
		this.yStepSize = yStepSize;
		this.xDirection = xDirection;
		this.yDirection = yDirection;
	}
	
	
	/**
	 * Moves the ball.
	 */
	public void move() {
		xPos += this.getXSpeed();
		yPos += this.getYSpeed();
	}
	
	
	/**
	 * Calculates and returns the ball's speed on the x axis.
	 * 
	 * @return xStepSize * xDirection	calculated speed of the ball in the direction of the xAxis
	 */
	public int getXSpeed() {
		return xStepSize * xDirection;
	}
	
	
	/**
	 * Calculates and returns the ball's speed on the y axis.
	 * 
	 * @return yStepSize * yDirection	calculated speed of the ball in the direction of the yAxis
	 */
	public int getYSpeed() {
		return yStepSize * yDirection;
	}
	
	/**
	 * Getter for xPos.
	 * 
	 * @return the ball's position on the x axis
	 */
	public int getXPos() {
		return xPos;
	}

	
	/**
	 * Setter for xPos.
	 * 
	 * @param xPos the ball's position on the x axis
	 */
	public void setXPos(int xPos) {
		this.xPos = xPos;
	}

	
	/**
	 * Getter for yPos.
	 * 
	 * @return the ball's position on the y axis
	 */
	public int getYPos() {
		return yPos;
	}

	
	/**
	 * Setter for yPos.
	 * 
	 * @param yPos the ball's position on the y axis
	 */
	public void setYPos(int yPos) {
		this.yPos = yPos;
	}

	/**
	 * Getter for diameter.
	 * 
	 * @return the ball's diameter
	 */
	public int getDiameter() {
		return diameter;
	}

	
	/**
	 * Setter for diameter.
	 * 
	 * @param diameter the ball's diameter
	 */
	public void setDiameter(int diameter) {
		this.diameter = diameter;
	}

	
	/**
	 * Getter for xStepSize.
	 * 
	 * @return amount the ball travels on the x axis in one game frame
	 */
	public int getXStepSize() {
		return xStepSize;
	}

	
	/**
	 * Setter for xStepSize.
	 * 
	 * @param xStepSize amount the ball travels on the x axis in one game frame
	 */
	public void setXStepSize(int xStepSize) {
		this.xStepSize = xStepSize;
	}

	
	/**
	 * Getter for yStepSize.
	 * 
	 * @return amount the ball travels on the y axis in one game frame
	 */
	public int getYStepSize() {
		return yStepSize;
	}

	
	/**
	 * Setter for yStepSize.
	 * 
	 * @param yStepSize amount the ball travels on the y axis in one game frame
	 */
	public void setYStepSize(int yStepSize) {
		this.yStepSize = yStepSize;
	}

	
	/**
	 * Getter for xDirection.
	 * 
	 * @return the ball's direction on the x axis
	 */
	public int getXDirection() {
		return xDirection;
	}

	
	/**
	 * Setter for xDirection.
	 * 
	 * @param xDirection the ball's direction on the x axis
	 */
	public void setXDirection(int xDirection) {
		this.xDirection = xDirection;
	}

	
	/**
	 * Getter for yDirection.
	 * 
	 * @return the ball's direction on the y axis
	 */
	public int getYDirection() {
		return yDirection;
	}

	
	/**
	 * Setter for yDirection.
	 * 
	 * @param yDirection the ball's direction on the y axis
	 */
	public void setYDirection(int yDirection) {
		this.yDirection = yDirection;
	}
}
