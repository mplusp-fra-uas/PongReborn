package pongReborn.application.gamemodels;

// TODO: comment the rest of the class

/**
 * Represents the player in the game.
 * 
 * @author Marco Peluso
 * @version 1.0
 */

public class Player {
	
	// constants for movement directions
	public static final int STOP = 0;
	public static final int UP = 1;
	public static final int DOWN = 2;
	
	private int xPos;
	private int yPos;
	private int width;
	private int height;
		
	private int speed;
	private int direction;
	
	private int topSegmentHeight;
	private int bottomSegmentHeight;
	
	
	/**
	 * Constructor. Creates a player object which can be used in the Pong game
	 * @param xPos						position of the player on the x axis
	 * @param yPos						position of the player on the y axis
	 * @param width						width of the paddle
	 * @param height					height of the paddle
	 * @param speed						movement speed of the paddle
	 * @param topSegmentHeight			declares the highest point of one of 8 paddle segments
	 * @param bottomSegmentHeight		declares the lowest point of one of 8 paddle segments
	 */
	public Player(int xPos, int yPos, int width, int height, int speed, int topSegmentHeight, int bottomSegmentHeight) {
		this.xPos = xPos;
		this.yPos = yPos;
		this.width = width;
		this.height = height;
		
		this.speed = speed;
		this.direction = STOP;
		
		this.topSegmentHeight = topSegmentHeight;
		this.bottomSegmentHeight = bottomSegmentHeight;
	}

	/**
	 * moves the player upwards on the y axis 
	 */
	public void moveUp() {
		yPos -= speed;
	}
	
	/**
	 * moves the player downwards on the y axis
	 */
	public void moveDown() {
		yPos += speed;
	}

	/**
	 * Getter for xPos
	 * @return xPos position of the player on the x axis
	 */
	public int getXPos() {
		return xPos;
	}
	
	/**
	 * Setter for xPos
	 * @param xPos new position for the player on the x axis
	 */
	public void setXPos(int xPos) {
		this.xPos = xPos;
	}

	/**
	 * Getter for yPos
	 * @return yPos position of the player on the y axis
	 */
	public int getYPos() {
		return yPos;
	}

	/**
	 * Setter for yPos
	 * @param yPos new position for the player on the y axis
	 */
	public void setYPos(int yPos) {
		this.yPos = yPos;
	}

	/**
	 * Getter for width
	 * @return width width of the player/paddle
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * Setter for width
	 * @param width new width for the player/paddle
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * Getter for height
	 * @return height height of the player/paddle
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * Setter for height
	 * @param height new height of the player/paddle
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * Getter for speed
	 * @return speed movement speed factor of the player/paddle
	 */
	public int getSpeed() {
		return speed;
	}

	/**
	 * Setter for speed 
	 * @param speed new movement speed factor for the player/paddle
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	/**
	 * Getter for direction
	 * @return direction direction in which the player/paddle is moving
	 */
	public int getDirection() {
		return direction;
	}
	
	/**
	 * Setter for direction
	 * @param direction new direction in which the player/paddle should move
	 */
	public void setDirection(int direction) {
		this.direction = direction;
	}

	/**
	 * Getter for topSegmentHeight
	 * @return topSegmentHeight the y position of the highest paddle segment out of eight
	 */
	public int getTopSegmentHeight() {
		return topSegmentHeight;
	}

	/**
	 * Setter for topSegmentHeight
	 * @param topSegmentHeight new y position of the highest paddle segment out of eight
	 */
	public void setTopSegmentHeight(int topSegmentHeight) {
		this.topSegmentHeight = topSegmentHeight;
	}

	/**
	 * Getter for bottomSegmentHeight
	 * @return bottomSegmentHeight the y position of the lowest paddle segment out of eight
	 */
	public int getBottomSegmentHeight() {
		return bottomSegmentHeight;
	}

	/**
	 * Setter for bottomSegmentHeight
	 * @param bottomSegmentHeight new y position of the lowest paddle segment out of eight
	 */
	public void setBottomSegmentHeight(int bottomSegmentHeight) {
		this.bottomSegmentHeight = bottomSegmentHeight;
	}
}
