package pongReborn.application.gamemodels;


/**
 * The game class contains the Pong game logic.
 * 
 * @author Steffen Hanzlik
 * @author Hendrik Mueller
 * @author Marco Peluso
 * @version 1.0
 */
public class Game {
	
	// defaults
	private static final int defaultWidth = 1000;
	private static final int defaultHeight = 700;
	
	private static final int defaultPlayerHeight = 128;
	private static final int defaultPlayerWidth = 25;
	private static final int defaultPlayerTopSegmentHeight = 16;
	private static final int defaultPlayerBottomSegmentHeight = 16;
	private static final int defaultPlayerSpeed = 12;
	
	private static final int defaultBallDiameter = 35;
	private static final int defaultStartBallXStepSize = 6;
	private static final int defaultStartBallYStepSize = 6;
	private static final int defaultMaxBallStepSize = 10;
	private static final int defaultBallXDirection = -1;
	private static final int defaultBallYDirection = -1;
	
	// class constants
	public static final int PLAYER_NONE_ID = -1;
	public static final int PLAYER_1_ID = 0;
	public static final int PLAYER_2_ID = 1;
	
	// game stuff
	private int width;
	private int height;

	// player stuff
	private Player[] players = new Player[2];
	private int[] playerScores = new int[2];

	// ball stuff
	private Ball ball;
	private int ballAccelerationStepSize;
	private int ballDiameter;

	private int startBallXPos;
	private int startBallYPos;
	private int startBallXStepSize;
	private int startBallYStepSize;
	private int maxBallStepSize;
	private int segment;


	/**
	 * Constructor. 
	 * 
	 * @param width width of the playing field
	 * @param height height of the playing field
	 * @param ball the game's ball
	 * @param ballAccelerationStepSize amount of ball acceleration
	 * @param player1 the first player
	 * @param player2 the second player
	 */
	public Game(int width, int height, Ball ball, int ballAccelerationStepSize, Player player1, Player player2) {
		this.width = width;
		this.height = height;
		
		playerScores[PLAYER_1_ID] = 0;
		playerScores[PLAYER_2_ID] = 0;

		this.ballAccelerationStepSize = 1;

		this.ball = ball;
		
		players[PLAYER_1_ID] = player1;
		players[PLAYER_2_ID] = player2;
	}

	
	/**
	 * Constructor creating a game with default values.
	 */
	public Game() {
		this.width = defaultWidth;
		this.height = defaultHeight;
		playerScores[PLAYER_1_ID] = 0;
		playerScores[PLAYER_2_ID] = 0;

		this.ballAccelerationStepSize = 1;

		// TODO: how/where to do/put this better?
		// ball parameters
		ballDiameter = defaultBallDiameter;
		startBallXPos = width / 2 - ballDiameter / 2;
		startBallYPos = height / 2 - ballDiameter / 2;
		startBallXStepSize = defaultStartBallXStepSize;
		startBallYStepSize = defaultStartBallYStepSize;
		maxBallStepSize = defaultMaxBallStepSize;

		this.ball = new Ball(width / 2 - ballDiameter / 2, height / 2 - ballDiameter / 2, ballDiameter, startBallXStepSize, startBallYStepSize, defaultBallXDirection, defaultBallYDirection);

		players[PLAYER_1_ID] = new Player(30, height / 2 - defaultPlayerHeight / 2, defaultPlayerWidth, defaultPlayerHeight, defaultPlayerSpeed, defaultPlayerTopSegmentHeight, defaultPlayerBottomSegmentHeight);
		players[PLAYER_2_ID] = new Player(width - defaultPlayerWidth - 30, height / 2 - defaultPlayerHeight / 2, defaultPlayerWidth, defaultPlayerHeight, defaultPlayerSpeed, defaultPlayerTopSegmentHeight, defaultPlayerBottomSegmentHeight);
	}
	
	
	/**
	 * Runs the game / calculates the next game frame.
	 * 
	 *  @return returns an int array with the format "{player1Score, player2Score, collisionPlayerId }"
	 */
	public int[] run() {
		int collisionPlayerId = detectCollisions();
		detectScoring();
		moveObjects();
		
		return new int[] { playerScores[PLAYER_1_ID], playerScores[PLAYER_2_ID], collisionPlayerId };
	}

	
	/**
	 * Accelerates the ball. 
	 */
	public void accelerateBall() {
		if (ball.getXStepSize() < maxBallStepSize) {
			ball.setXStepSize(ball.getXStepSize() + ballAccelerationStepSize);
		}
		if (ball.getYStepSize() < maxBallStepSize) {
			ball.setYStepSize(ball.getYStepSize() + ballAccelerationStepSize);
		}
	}

	
	/**
	 * Resets the ball. 
	 */
	public void resetBall() {
		ball.setXPos(startBallXPos);
		ball.setYPos(startBallYPos);

		ball.setXDirection(ball.getXDirection() * -1);
		ball.setXStepSize(startBallXStepSize);
		ball.setYStepSize(startBallYStepSize);
	}

	
	/**
	 * Handles collision detection. 
	 * 
	 * @return returns the id of the player hitting the ball.
	 */
	public int detectCollisions() {
		
		detectCollisionBallWithTopOfPlayingField();
		detectCollisionBallWithBottomOfPlayingField();
		
		boolean player1Collided = detectCollisionBallWithPlayer1();
		boolean player2Collided = detectCollisionBallWithPlayer2();
		
		if (player1Collided) {
			return PLAYER_1_ID;
		} else if (player2Collided) {
			return PLAYER_2_ID;
		} else {
			return PLAYER_NONE_ID;
		}
	}
	
	/**
	 * Detects if player1 did score.
	 * 
	 * @return returns true, if player 1 scored
	 */
	public boolean detectPlayer1Scoring() {
		
		boolean playerScored = false;

		// collision detection: right end of playing field
		if (ball.getXPos() >= width - ball.getDiameter()) {
			
			playerScored = true;
			
			playerScores[PLAYER_1_ID]++;
			resetBall();
		}
		
		return playerScored;
	}

	
	/**
	 * Detects if player2 did score.
	 * 
	 * @return returns true, if player 1 scored
	 */
	public boolean detectPlayer2Scoring() {
		
		boolean playerScored = false;

		// collision detection: left end of playing field
		if (ball.getXPos() <= 0) {
			
			playerScored = true;
			playerScores[PLAYER_2_ID]++;
			resetBall();
		}
		
		return playerScored;
	}
	
	
	/**
	 * Handles scoring.
	 * 
	 *  @return returns playerId of player that did score.
	 */
	public int detectScoring() {
		
		boolean player1Scored = detectPlayer1Scoring();
		boolean player2Scored = detectPlayer2Scoring();
		
		if (player1Scored) {
			return PLAYER_1_ID;
		} else if (player2Scored) {
			return PLAYER_2_ID;
		} else {
			return PLAYER_NONE_ID;
		}
		
	}

	
	/**
	 * Handles collision detection (ball with top of playing field).
	 */
	public void detectCollisionBallWithTopOfPlayingField() {
		if (ball.getYPos() <= 0) {

			ball.setYPos(0); // prevent ball getting stuck in ceiling
			ball.setYDirection(ball.getYDirection() * -1);
		}
	}
	

	/**
	 * Handles collision detection (ball with bottom of playing field).
	 */
	public void detectCollisionBallWithBottomOfPlayingField() {
		if (ball.getYPos() >= height - ball.getDiameter()) {
			
			ball.setYPos(height - ball.getDiameter());// prevent ball getting stuck in floor
			ball.setYDirection(ball.getYDirection() * -1);
		}
	}

	
	/**
	 * Handles collision detection (ball with player1).
	 * 
	 * @return returns true if collision with ball did happen!
	 */
	public boolean detectCollisionBallWithPlayer1() {
		
		boolean playerCollided = false;
		
		if (ball.getXPos() <= players[PLAYER_1_ID].getXPos() + players[PLAYER_1_ID].getWidth() &&
				ball.getYPos() + ball.getDiameter() >= players[PLAYER_1_ID].getYPos() &&
				ball.getYPos() <= players[PLAYER_1_ID].getYPos() + players[PLAYER_1_ID].getHeight()) {
			
			playerCollided = true;

			// prevent ball getting stuck in paddle
			ball.setXPos(players[PLAYER_1_ID].getXPos() + players[PLAYER_1_ID].getWidth());

			accelerateBall();

			// bounce back ball
			segment =  players[PLAYER_1_ID].getHeight() / 8;
			
			// bounce back ball
			if (ball.getYPos() + ball.getDiameter() <= players[PLAYER_1_ID].getYPos() + segment ) {
				// if top segment is hit, bounce back in a -45 degree angle
				ball.setXDirection(1);
				ball.setYDirection(-1);
				ball.setXStepSize(7);
				ball.setYStepSize(7);
//				System.out.println("***************************************-3");

			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_1_ID].getYPos() + segment && ball.getYPos() + ball.getDiameter () <= players[PLAYER_1_ID].getYPos() + (2*segment) ) {
				// if -2 segmentFromTop is hit, bounce back in -35 degree angle
				ball.setXDirection(1);
				ball.setYDirection(-1);
				ball.setXStepSize(8);
				ball.setYStepSize(5);
//				System.out.println("****************************************-2");
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_1_ID].getYPos() + (2*segment) && ball.getYPos() + ball.getDiameter () <= players[PLAYER_1_ID].getYPos() + (4*segment) ) {
				//if -1 segmentFromTop is hit, bounce back in -15 degree angle
				ball.setXDirection(1);
				ball.setYDirection(-1);
				ball.setXStepSize(8);
				ball.setYStepSize(3);
//				System.out.println("************************************-1");
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_1_ID].getYPos() + (4*segment) && ball.getYPos() + ball.getDiameter () <= players[PLAYER_1_ID].getYPos() + (5*segment)) {
				// if middle segment is hit, bounce back in a +0 degree angle
				ball.setXDirection(ball.getXDirection() * -1);
				ball.setYDirection(0);
				ball.setXStepSize(8);
				ball.setYStepSize(8);
//				System.out.println("0");
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_1_ID].getYPos() + (5*segment) && ball.getYPos() + ball.getDiameter () <= players[PLAYER_1_ID].getYPos() + (6*segment)) {	
				//if 2 segmentFromTop is hit, bounce back in 15 degree angle
				ball.setXDirection(1);
				ball.setYDirection(1);
				ball.setXStepSize(8);
				ball.setYStepSize(3);
//				System.out.println("1");
			
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_1_ID].getYPos() + (6*segment) && ball.getYPos() + ball.getDiameter () <= players[PLAYER_1_ID].getYPos() + (8*segment)) {	
				//if 2 segmentFromTop is hit, bounce back in 35 degree angle
				ball.setXDirection(1);
				ball.setYDirection(1);
				ball.setXStepSize(8);
				ball.setYStepSize(5);
//				System.out.println("2");
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_1_ID].getYPos() + (8*segment)) {
				// if bottom segment is hit, bounce back in a +45 degree angle
				ball.setXDirection(1);
				ball.setYDirection(1);
				ball.setXStepSize(7);
				ball.setYStepSize(7);
//				System.out.println("3");
			}
		}
		
		return playerCollided;
	}

	
	/**
	 * Handles collision detection (ball with player2).
	 * 
	 * @return returns true if collision with ball did happen!
	 */
	public boolean detectCollisionBallWithPlayer2() {
		
		boolean playerCollided = false;
		
		if (ball.getXPos() >= players[PLAYER_2_ID].getXPos() - ball.getDiameter() &&
				ball.getYPos() + ball.getDiameter() >= players[PLAYER_2_ID].getYPos() &&
				ball.getYPos() <= players[PLAYER_2_ID].getYPos() + players[PLAYER_2_ID].getHeight()) {
			
			playerCollided = true;

			// prevent ball getting stuck in paddle

			accelerateBall();
			
			segment = players[PLAYER_2_ID].getHeight() /8;
			// bounce back ball
			if (ball.getYPos() + ball.getDiameter() <= players[PLAYER_2_ID].getYPos() + 1*segment ) {
				// if top segment is hit, bounce back in a -45 degree angle
				ball.setXDirection(-1);
				ball.setYDirection(-1);
				ball.setXStepSize(7);
				ball.setYStepSize(7);
//				System.out.println("***********************************************************-3");

			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_2_ID].getYPos() + 1*segment && ball.getYPos() + ball.getDiameter () <= players[PLAYER_2_ID].getYPos() + (2*segment) ) {
				// if -2 segmentFromTop is hit, bounce back in -35 degree angle
				ball.setXDirection(-1);
				ball.setYDirection(-1);
				ball.setXStepSize(8);
				ball.setYStepSize(3);
//				System.out.println("***********************************************************-2");
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_2_ID].getYPos() + (2*segment) && ball.getYPos() + ball.getDiameter () <= players[PLAYER_2_ID].getYPos() + (3*segment) ) {
				//if -1 segmentFromTop is hit, bounce back in -15 degree angle
				ball.setXDirection(-1);
				ball.setYDirection(-1);
				ball.setXStepSize(8);
				ball.setYStepSize(5);
//				System.out.println("*************************************************************-1");
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_2_ID].getYPos() + (3*segment) && ball.getYPos() + ball.getDiameter () <= players[PLAYER_2_ID].getYPos() + (4*segment)) {
				// if middle segment is hit, bounce back in a +0 degree angle
				ball.setXDirection(ball.getXDirection() * -1);
				ball.setYDirection(0);
				ball.setXStepSize(8);
				ball.setYStepSize(8);
//				System.out.println("0");
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_2_ID].getYPos() + (4*segment) && ball.getYPos() + ball.getDiameter () <= players[PLAYER_2_ID].getYPos() + (6*segment)) {	
				//if 2 segmentFromTop is hit, bounce back in 15 degree angle
				ball.setXDirection(-1);
				ball.setYDirection(1);
				ball.setXStepSize(8);
				ball.setYStepSize(5);
//				System.out.println("1");
			
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_2_ID].getYPos() + (6*segment) && ball.getYPos() + ball.getDiameter () <= players[PLAYER_2_ID].getYPos() + (8*segment)) {	
				//if 2 segmentFromTop is hit, bounce back in 35 degree angle
				ball.setXDirection(-1);
				ball.setYDirection(1);
				ball.setXStepSize(8);
				ball.setYStepSize(3);
//				System.out.println("2");
			} else if (ball.getYPos() + ball.getDiameter () > players[PLAYER_2_ID].getYPos() + (8*segment)) {
				// if bottom segment is hit, bounce back in a +45 degree angle
				ball.setXDirection(-1);
				ball.setYDirection(1);
				ball.setXStepSize(7);
				ball.setYStepSize(7);
//				System.out.println("3");
			}
		}
		
		return playerCollided;
	}		

	
	/**
	 * Moves player1 depending on player1's direction.
	 */
	public void movePlayer1() {
		// move player1 depending on current playerOneDirection
		if (players[PLAYER_1_ID].getDirection() == Player.UP) {
			if (players[PLAYER_1_ID].getYPos() - players[PLAYER_1_ID].getSpeed() > 0) {
				players[PLAYER_1_ID].moveUp();
			} else {
				players[PLAYER_1_ID].setYPos(0);
			}
		} else if (players[PLAYER_1_ID].getDirection() == Player.DOWN) {
			if (players[PLAYER_1_ID].getYPos() + players[PLAYER_1_ID].getSpeed() < height - players[PLAYER_1_ID].getHeight()) {
				players[PLAYER_1_ID].moveDown();
			} else {
				players[PLAYER_1_ID].setYPos(height - players[PLAYER_1_ID].getHeight());
			}
		}
	}

	
	/**
	 * Moves player2 depending on player1's direction.
	 */
	public void movePlayer2() {
		//move player2 depending on current playerTwoDirection
		if (players[PLAYER_2_ID].getDirection() == Player.UP) {
			if (players[PLAYER_2_ID].getYPos() - players[PLAYER_2_ID].getSpeed() > 0) {
				players[PLAYER_2_ID].moveUp();
			} else {
				players[PLAYER_2_ID].setYPos(0);
			}
		} else if (players[PLAYER_2_ID].getDirection() == Player.DOWN) {
			if (players[PLAYER_2_ID].getYPos() < height - players[PLAYER_2_ID].getHeight()) {
				players[PLAYER_2_ID].moveDown();
			} else {
				players[PLAYER_2_ID].setYPos(height - players[PLAYER_2_ID].getHeight());
			}
		}
	}

	
	/**
	 * Moves relevant objects in the game.
	 */
	public void moveObjects() {
		ball.move();
		movePlayer1();
		movePlayer2();
	}	

	
	// TODO: comment the rest of the methods
	/**
	 * Getter for width.
	 * 
	 * @return width of the playing field
	 */
	public int getWidth() {
		return width;
	}

	
	/**
	 * Setter for width.
	 * 
	 * @param width width of the playing field
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	
	/**
	 * Getter for height.
	 * 
	 * @return height of the playing field
	 */
	public int getHeight() {
		return height;
	}

	
	/**
	 * Setter for height.
	 * 
	 * @param height height of the playing field
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	
	/**
	 * Getter for ball.
	 * 
	 * @return ball the game's ball
	 */
	public Ball getBall() {
		return ball;
	}

	
	/**
	 * Setter for ball.
	 * 
	 * @param ball the game's ball
	 */
	public void setBall(Ball ball) {
		this.ball = ball;
	}

	
	/**
	 * Getter for player1
	 * 
	 * @return player1 first player/paddle
	 */
	public Player getPlayer1() {
		return players[PLAYER_1_ID];
	}

	
	/**
	 * Setter for player1
	 * @param player1 new player/paddle
	 */
	public void setPlayer1(Player player1) {
		this.players[PLAYER_1_ID] = player1;
	}

	
	/**
	 * Getter for player2
	 * @return player2 second player/paddle
	 */
	public Player getPlayer2() {
		return players[PLAYER_2_ID];
	}

	
	/**
	 * Setter for player2
	 * @param player2 new player/paddle
	 */
	public void setPlayer2(Player player2) {
		this.players[PLAYER_2_ID] = player2;
	}

	
	/**
	 * Getter for ballAccelerationStepSize
	 * @return ballAccelerationStepSize ballAccelerationStepSize of the ball
	 */
	public int getBallAccelerationStepSize() {
		return ballAccelerationStepSize;
	}

	
	/**
	 * Setter for ballAccelerationStepSize
	 * @param ballAccelerationStepSize new ballAccelerationStepSize of the ball
	 */
	public void setBallAccelerationStepSize(int ballAccelerationStepSize) {
		this.ballAccelerationStepSize = ballAccelerationStepSize;
	}

	
	/**
	 * Getter for ballDiameter
	 * @return ballDiameter diameter of the ball
	 */
	public int getBallDiameter() {
		return ballDiameter;
	}

	
	/**
	 * Setter for ballDiameter
	 * @param ballDiameter new diameter for the ball
	 */
	public void setBallDiameter(int ballDiameter) {
		this.ballDiameter = ballDiameter;
	}

	
	/**
	 * Getter for startBallXStepSize
	 * @return startBallXStepSize movement factor on x axis per frame
	 */
	public int getStartBallXStepSize() {
		return startBallXStepSize;
	}

	
	/**
	 * Setter for startBallXStepSize
	 * @param startBallXStepSize new movement factor on x axis per frame
	 */
	public void setStartBallXStepSize(int startBallXStepSize) {
		this.startBallXStepSize = startBallXStepSize;
	}

	
	/**
	 * Getter for startBallYStepSize
	 * @return startBallYStepSize movement factor on y axis per frame
	 */
	public int getStartBallYStepSize() {
		return startBallYStepSize;
	}

	
	/**
	 * Setter for startBallYStepSize
	 * @param startBallYStepSize new movement factor on y axis per frame
	 */
	public void setStartBallYStepSize(int startBallYStepSize) {
		this.startBallYStepSize = startBallYStepSize;
	}

	
	/**
	 * Getter for playerScores
	 * @return int array containing the player scores
	 */
	public int[] getPlayerScores() {
		return playerScores;
	}
	
	
	/**
	 * Getter for players
	 * @return int array containing the players
	 */
	public Player[] getPlayers() {
		return players;
	}
	
	
	/**
	 * Setter for players
	 * @param players int array containing the players
	 */
	public void setPlayers(Player[] players) {
		this.players = players;
	}
	
	
	/**
	 * Setter for playerScores
	 * @param playerScores int array containing the player scores
	 */
	public void setPlayerScores(int[] playerScores) {
		this.playerScores = playerScores;
	}
	
	
	/**
	 * Getter for startBallXPos
	 * @return startBallXPos position of the ball on x axis when starting the game
	 */
	public int getStartBallXPos() {
		return startBallXPos;
	}
	
	
	/**
	 * Setter for startBallXPos
	 * @param startBallXPos new position for the ball on x axis when starting the games
	 */
	public void setStartBallXPos(int startBallXPos) {
		this.startBallXPos = startBallXPos;
	}
	
	
	/**
	 * Getter for startBallYPos
	 * @return startBallYPos position of the ball on y axis when starting the game
	 */
	public int getStartBallYPos() {
		return startBallYPos;
	}

	
	/**
	 * Setter for startBallYPos
	 * @param startBallYPos new position for the ball on y axis when starting the game
	 */
	public void setStartBallYPos(int startBallYPos) {
		this.startBallYPos = startBallYPos;
	}
	
}
